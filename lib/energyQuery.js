const moment = require('moment')

// Helpers
const { joinItemsWithCommaForQuery } =  require('./helpers/dataHelpers')
const {
  getFormatDateSqlByGranularity,
  getAllWeeks,
  getAllYears,
  getDateArrayInSqlFormat,
  getDateArrayInMonthAndYear 
} =  require('./helpers/granularityHelpers')

// bigQuery
const { runQuery, getFrom, getProjectId } = require('aurora-bigquery/lib/bigQuery')

// Query gets last warehouse by elements
module.exports.findLastWarehouseByElements = function findLastConsumptionProfileByElements({ dataSetName, elementsId }) {
  const tableName = 'warehouse'
  const query = `
  SELECT elementId, 
  FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S",ARRAY_AGG(STRUCT(createdAt) ORDER BY createdAt DESC LIMIT 1)[offset(0)].createdAt) createdAt
  FROM  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND createdAt IS NOT NULL
  AND consumption IS NOT NULL 
  GROUP BY elementId`
  return runQuery(query)
}

// Query gets last consumption_profile by elements
module.exports.findConsumptionProfileLastHour = function findConsumptionProfileLastHour({ dataSetName, elementId }) {
  const tableName = 'consumption_profile_hour'
  const query = `
    SELECT elementId, consumptionProfile, FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S", loadStoringTime) loadStoringTime
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementId}'
    AND consumptionProfile IS NOT NULL
    ORDER BY loadStoringTime DESC
    LIMIT 1 `

  return runQuery(query)  
}


module.exports.findConsumptionProfileLastDaily = function findConsumptionProfileLastDaily({ dataSetName, elementId }) {
  const tableName = 'consumption_profile_daily'
  const query = `
    SELECT elementId, consumptionProfile, FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S", loadStoringTime) loadStoringTime
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementId}'
    AND consumptionProfile IS NOT NULL
    ORDER BY loadStoringTime DESC
    LIMIT 1 `

  return runQuery(query)  
}
  
module.exports.findConsumptionProfileLastWeekly = function findConsumptionProfileLastWeekly({ dataSetName, elementId }) {
  const tableName = 'consumption_profile_weekly'
  const query = `
     SELECT elementId, consumptionProfile, year, week 
     FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
     WHERE elementId = '${elementId}'
     AND consumptionProfile IS NOT NULL
     ORDER BY year DESC, week DESC 
     LIMIT 1 `

  return runQuery(query)  
}
  
module.exports.findConsumptionProfileLastMmnthly = function findLastConsumptionProfileByElements({ dataSetName, elementId }) {
  const tableName = 'consumption_profile_monthly'
  const query = `
  SELECT elementId, consumptionProfile, year, month 
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND consumptionProfile IS NOT NULL
  ORDER BY year DESC, month DESC 
  LIMIT 1 `

  return runQuery(query)  
}

// LOAD PROFILE - DAY
module.exports.findLoadProfilePerDay = function findLoadProfilePerDay({ dataSetName, elementsId, date, limit, isReactive }) {
  const tableName = 'consumption_profile_daily'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const query = `SELECT DISTINCT elementId, ${field}, loadStoringTime, day, weekday, month, year, '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
   ORDER BY
    loadStoringTime ASC
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
  .catch((err) => {
       console.error(err)
      return []
     }
  )
}

// LOAD PROFILE - WEEK
module.exports.findLoadProfilePerWeek = function findLoadProfilePerWeek({ dataSetName, elementsId, date, limit, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const tableName = 'consumption_profile_weekly'
  const query = `SELECT DISTINCT elementId, ${field}, week, year, '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllWeeks({ date })})
  ORDER BY
    year ASC,
    week ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query)
  .catch((err) => {
    console.error(err)
   return []
   }
  )
}
// LOAD PROFILE - DAY
module.exports.findLoadProfilePerDay = function findLoadProfilePerDay({ dataSetName, elementsId, date, limit, isReactive }) {
  const tableName = 'consumption_profile_daily'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const query = `SELECT DISTINCT elementId, ${field}, loadStoringTime, day, weekday, month, year, '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
   ORDER BY
    loadStoringTime ASC
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
  .catch((err) => {
       console.error(err)
      return []
     }
  )
}

// LOAD PROFILE - WEEK
module.exports.findLoadProfilePerWeek = function findLoadProfilePerWeek({ dataSetName, elementsId, date, limit, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const tableName = 'consumption_profile_weekly'
  const query = `SELECT DISTINCT elementId, ${field}, week, year, '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllWeeks({ date })})
  ORDER BY
    year ASC,
    week ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query)
  .catch((err) => {
    console.error(err)
   return []
   }
  )
}


// LOAD PROFILE - MONTH
module.exports.findLoadProfilePerMonth = function findLoadProfilePerMonth({ dataSetName, elementsId, date, limit, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const tableName = 'consumption_profile_monthly'
  const query = `SELECT  DISTINCT 
      elementId,
      ${field},
      month,
      year,
    '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getDateArrayInMonthAndYear({ dates: date })})
   ORDER BY
    year,
    month
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query)
  .catch((err) => {
    console.error(err)
    return []
  })
}
//get goals value by month and year
module.exports.findValuesGoalByMonthAndYear = function findValuesGoalByMonthAndYear({
  dataSetName,
  locationId,
  goalId,
  tableName = 'goal_monthly',
  year,
  fields = 'locationId, elementId, goalId, value, month, year'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    locationId IN (${getElementsIdForQuery(locationId)})
    AND goalId = '${goalId}'
    AND year=${year}
    ORDER BY month
  `
  return runQuery(query)
}

// LOAD PROFILE - YEAR
module.exports.findLoadProfilePerYear = function findLoadProfilePerYear({ dataSetName, elementsId, date, limit, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const tableName = 'consumption_profile_annual'
  const query = `SELECT DISTINCT
    elementId,
    ${field},
    year, 
    '${field}' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllYears({ date, withOutDay: true })})
  ORDER BY
    year ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).catch(() => [])
}

// CONSUMPTION - DAY
module.exports.findConsumptionPerDay = function findConsumptionPerDay({ dataSetName, elementsId, date, limit, isReactive }) {
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const tableName = 'consumption_daily'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const query = `SELECT DISTINCT
   elementId,
   identificationCode,
   ${field},
   loadStoringTime,
   day,
   month,
   weekday,
   week,
   year,
   '${field}' as metric
   FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
   ORDER BY
     year ASC,
     month ASC,
     day ASC
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
    .catch(() => {
      return []
    })
}
//this query gets the value of goals
module.exports.findValuesGoalByMonthAndYear = function findValuesGoalByMonthAndYear({
  dataSetName,
  locationId,
  goalId,
  tableName = 'goals_monthly',
  year,
  fields = 'locationId, goalId, value, month, year'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE
    locationId IN (${joinItemsWithCommaForQuery(locationId)})
    AND goalId = '${goalId}'
    AND year=${year}
    ORDER BY month
  `
  return runQuery(query)
}


// CONSUMPTION - WEEK
module.exports.findConsumptionPerWeek = function findConsumptionPerWeek({ dataSetName, elementsId, date, limit, isReactive }) {
  const tableName = 'consumption_daily'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query = `SELECT DISTINCT 
      elementId,
      identificationCode, 
      ${field}, 
      loadStoringTime, 
      day, 
      month, 
      week, 
      year, 
      '${field}' as metric
      FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllWeeks({ date })})
  AND weekday = 7
   ORDER BY
     year ASC,
     week ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).catch((err) => {
    console.error(err)
    return []
  })
}


// CONSUMPTION - MONTH
module.exports.findConsumptionPerMonth = function findConsumptionPerMonth({ dataSetName, elementsId, date, limit, isReactive }) {
  const tableName = 'consumption_daily'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query = `SELECT DISTINCT 
    elementId,
    identificationCode,
    ${field},
    loadStoringTime,
    day,
    month,
    weekday,
    week,
    year,
    '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND loadStoringTime IN (${getDateArrayInSqlFormat({ dates: date })})
   ORDER BY
    loadStoringTime
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).catch((err) => {
    console.error(err)
    return []
  })
}

// LOAD PROFILE - YEAR
module.exports.findLoadProfilePerYear = function findLoadProfilePerYear({ dataSetName, elementsId, date, limit, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const tableName = 'consumption_profile_annual'
  const query = `SELECT DISTINCT
    elementId,
    ${field},
    year, 
    '${field}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllYears({ date, withOutDay: true })})
  ORDER BY
    year ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).catch((err) => {
    console.error(err)
    return []
  })
}

/**
 * This query gets voltage per Instantaneous between a date range 
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {string} startDate: Start date of range
 * @param {string} endDate: End date of range
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   voltage,
 *   voltagePhase1,
 *   voltagePhase2,
 *   voltagePhase3,
 * },
 * .....
 * ]
 */
module.exports.findVoltageInstantaneous = function findVoltageInstantaneous({ 
dataSetName, elementsId, startDate, endDate, limit, metric = 'average_phase_voltage' }) {
  const tableName = 'electrical_measurements'
  const query = `
  SELECT DISTINCT 
    elementId, 
    DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime, 
      voltage,
      voltagePhase1,
      voltagePhase2,
      voltagePhase3,
     '${metric}' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  ORDER BY
    loadStoringTime 
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
}

/**
 * This query gets voltage per hour, day, week monthly and between a date range 
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {string} startDate: Start date of range
 * @param {string} endDate: End date of range
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   voltage,
 *   voltagePhase1,
 *   voltagePhase2,
 *   voltagePhase3,
 * },
 * .....
 * ]
 */
 module.exports.findVoltagePerHourAndDayAndWeekAndMonthAndYear = 
 function findCurrentPerHourAndDayAndWeekAndMonthAndYear({
 dataSetName, elementsId, startDate, endDate, granularity, limit,
 metric = 'average_phase_voltage'}) {
   const tableName = 'electrical_measurements'
   const formatDate = getFormatDateSqlByGranularity(granularity)
   const query = `
   SELECT elementId,
       AVG(voltage) AS voltage,
       MAX(voltage) AS voltageMax,
       MIN(voltage) AS voltageMin,
       AVG(voltagePhase1) AS voltagePhase1,
       MAX(voltagePhase1) AS voltagePhase1Max,
       MIN(voltagePhase1) AS voltagePhase1Min,
       AVG(voltagePhase2) AS voltagePhase2,
       MAX(voltagePhase2) AS voltagePhase2Max,
       MIN(voltagePhase2) AS voltagePhase2Min,
       AVG(voltagePhase3) AS voltagePhase3,
       MAX(voltagePhase3) AS voltagePhase3Max,
       MIN(voltagePhase3) AS voltagePhase3Min,
       FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
       '${metric}' AS metric
   FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
   WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
     AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
   GROUP BY
     elementId,
     loadStoringTime
    ORDER BY
     loadStoringTime DESC 
   ${limit ? `LIMIT ${limit}` : ''}`
 
   return runQuery(query)
 }

 

/**
 * This query gets voltage per Instantaneous between a date range 
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {string} startDate: Start date of range
 * @param {string} endDate: End date of range
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   current,
 *   currentPhase1,
 *   currentPhase2,
 *   currentPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findCurrentInstantaneous = function findcurrentInstantaneous({ 
  dataSetName, elementsId, startDate, endDate, limit, metric = 'average_phase_current' }) {
    const tableName = 'electrical_measurements'
    const query = `
    SELECT DISTINCT 
      elementId, 
      DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
          EXTRACT(MONTH FROM loadStoringTime), 
          EXTRACT(DAY FROM loadStoringTime), 
          EXTRACT(HOUR FROM loadStoringTime), 
          EXTRACT(MINUTE FROM loadStoringTime), 
          0
          ) loadStoringTime, 
        electricalCurrent,
        currentPhase1,
        currentPhase2,
        currentPhase3,
       '${metric}' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    ORDER BY
      loadStoringTime 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }


/**
 * This query gets current per hour, day, week monthly and between a date range 
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {string} startDate: Start date of range
 * @param {string} endDate: End date of range
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   current,
 *   currentPhase1,
 *   currentPhase2,
 *   currentPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findCurrentPerHourAndDayAndWeekAndMonthAndYear = 
 function findCurrentPerHourAndDayAndWeekAndMonthAndYear({
 dataSetName, elementsId, startDate, endDate, granularity, limit,
 metric = 'average_phase_current'}) {
   const tableName = 'electrical_measurements'
   const formatDate = getFormatDateSqlByGranularity(granularity)
   const query = `
   SELECT elementId,
       AVG(electricalCurrent) AS electricalCurrent,
       MAX(electricalCurrent) AS currentMax,
       MIN(electricalCurrent) AS currentMin,
       AVG(currentPhase1) AS currentPhase1,
       MAX(currentPhase1) AS currentPhase1Max,
       MIN(currentPhase1) AS currentPhase1Min,
       AVG(currentPhase2) AS currentPhase2,
       MAX(currentPhase2) AS currentPhase2Max,
       MIN(currentPhase2) AS currentPhase2Min,
       AVG(currentPhase3) AS currentPhase3,
       MAX(currentPhase3) AS currentPhase3Max,
       MIN(currentPhase3) AS currentPhase3Min,
       FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
       '${metric}' AS metric
   FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
   WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
     AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
   GROUP BY
     elementId,
     loadStoringTime
    ORDER BY
     loadStoringTime DESC 
   ${limit ? `LIMIT ${limit}` : ''}`
 
   return runQuery(query)
 }



/**
 * This query gets powerFactor per Instantaneous between a date range 
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {string} startDate: Start date of range
 * @param {string} endDate: End date of range
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   powerFactor,
 *   powerFactorPhase1,
 *   powerFactorPhase2,
 *   powerFactorPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findPowerFactorInstantaneous = function findPowerFactorInstantaneous({ 
  dataSetName, elementsId, startDate, endDate, limit, metric = 'average_phase_powerFactor' }) {
    const tableName = 'electrical_measurements'
    const query = `
    SELECT DISTINCT 
      elementId, 
      DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
          EXTRACT(MONTH FROM loadStoringTime), 
          EXTRACT(DAY FROM loadStoringTime), 
          EXTRACT(HOUR FROM loadStoringTime), 
          EXTRACT(MINUTE FROM loadStoringTime), 
          0
          ) loadStoringTime, 
          totalPowerFactor powerFactor,
          totalPowerFactorPhase1 powerFactorPhase1,
          totalPowerFactorPhase2 powerFactorPhase2,
          totalPowerFactorPhase3 powerFactorPhase3,
       '${metric}' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    ORDER BY
      loadStoringTime 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }


/**
 * This query gets current per hour, day, week monthly and between a date range 
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {string} startDate: Start date of range
 * @param {string} endDate: End date of range
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   powerFactor,
 *   powerFactorPhase1,
 *   powerFactorPhase2,
 *   powerFactorPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findPowerFactorPerHourAndDayAndWeekAndMonthAndYear = 
 function findPowerFactorPerHourAndDayAndWeekAndMonthAndYear({
 dataSetName, elementsId, startDate, endDate, granularity, limit,
 metric = 'average_phase_powerFactor'}) {
   const tableName = 'electrical_measurements'
   const formatDate = getFormatDateSqlByGranularity(granularity)
   const query = `
   SELECT elementId,
       AVG(totalPowerFactor) AS powerFactor,
       MAX(totalPowerFactor) AS powerFactorMax,
       MIN(totalPowerFactor) AS powerFactorMin,
       AVG(totalPowerFactorPhase1) AS powerFactorPhase1,
       MAX(totalPowerFactorPhase2) AS powerFactorPhase1Max,
       MIN(totalPowerFactorPhase3) AS powerFactorPhase1Min,
       AVG(totalPowerFactorPhase1) AS powerFactorPhase2,
       MAX(totalPowerFactorPhase2) AS powerFactorPhase2Max,
       MIN(totalPowerFactorPhase3) AS powerFactorPhase2Min,
       AVG(totalPowerFactorPhase1) AS powerFactorPhase3,
       MAX(totalPowerFactorPhase2) AS powerFactorPhase3Max,
       MIN(totalPowerFactorPhase3) AS powerFactorPhase3Min,
       FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
       '${metric}' AS metric
   FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
   WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
     AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
   GROUP BY
     elementId,
     loadStoringTime
    ORDER BY
     loadStoringTime DESC 
   ${limit ? `LIMIT ${limit}` : ''}`
 
   return runQuery(query)
 }

/**
 * This query gets voltage per Instantaneous between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   voltage,
 *   voltagePhase1,
 *   voltagePhase2,
 *   voltagePhase3,
 * },
 * .....
 * ]
 */
 module.exports.findVoltageCompareInstantaneous = function findVoltageCompareInstantaneous({ 
  dataSetName, elementsId, limit, metric = 'average_phase_voltage',rangeOne, rangeTwo }) {
    const tableName = 'electrical_measurements'
    const query = `
    SELECT DISTINCT 
        elementId, 
        DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime,
        voltage,
        voltagePhase1,
        voltagePhase2,
        voltagePhase3, 
        '${metric}' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementsId}'
    AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
    OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
     ORDER BY
     loadStoringTime 
   ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

/**
 * This query gets voltage per Instantaneous between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   voltage,
 *   voltagePhase1,
 *   voltagePhase2,
 *   voltagePhase3,
 * },
 * .....
 * ]
 */
 module.exports.findComparePerHourAndDayAndWeekAndMonthAndYear = 
 function findComparePerHourAndDayAndWeekAndMonthAndYear({ 
  dataSetName, elementsId, granularity, limit, metric = 'average_phase_voltage',rangeOne, rangeTwo }) {
    const tableName = 'electrical_measurements'
    const formatDate = getFormatDateSqlByGranularity(granularity)
    const query = `
    SELECT elementId,
        AVG(voltage) AS voltage,
        MAX(voltage) AS voltageMax,
        MIN(voltage) AS voltageMin,
        AVG(voltagePhase1) AS voltagePhase1,
        MAX(voltagePhase1) AS voltagePhase1Max,
        MIN(voltagePhase1) AS voltagePhase1Min,
        AVG(voltagePhase2) AS voltagePhase2,
        MAX(voltagePhase2) AS voltagePhase2Max,
        MIN(voltagePhase2) AS voltagePhase2Min,
        AVG(voltagePhase3) AS voltagePhase3,
        MAX(voltagePhase3) AS voltagePhase3Max,
        MIN(voltagePhase3) AS voltagePhase3Min,
        FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
        '${metric}' AS metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementsId}' 
      AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
      OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
    GROUP BY
      elementId,
      loadStoringTime
     ORDER BY
      loadStoringTime DESC 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }


/**
 * This query gets current per Instantaneous between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   current,
 *   currentPhase1,
 *   currentPhase2,
 *   currentPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findCurrentComparePerHourAndDayAndWeekAndMonthAndYear = 
 function findComparePerHourAndDayAndWeekAndMonthAndYear({ 
  dataSetName, elementsId, granularity, limit, metric = 'average_phase_current',rangeOne, rangeTwo }) {
    const tableName = 'electrical_measurements'
    const formatDate = getFormatDateSqlByGranularity(granularity)
    const query = `
    SELECT elementId,
        AVG(electricalCurrent) AS electricalCurrent,
        MAX(electricalCurrent) AS currentMax,
        MIN(electricalCurrent) AS currentMin,
        AVG(currentPhase1) AS currentPhase1,
        MAX(currentPhase1) AS currentPhase1Max,
        MIN(currentPhase1) AS currentPhase1Min,
        AVG(currentPhase2) AS currentPhase2,
        MAX(currentPhase2) AS currentPhase2Max,
        MIN(currentPhase2) AS currentPhase2Min,
        AVG(currentPhase3) AS currentPhase3,
        MAX(currentPhase3) AS currentPhase3Max,
        MIN(currentPhase3) AS currentPhase3Min,
        FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
        '${metric}' AS metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementsId}' 
      AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
      OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
    GROUP BY
      elementId,
      loadStoringTime
     ORDER BY
      loadStoringTime DESC 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

/**
 * This query gets current per Instantaneous between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   current,
 *   currentPhase1,
 *   currentPhase2,
 *   currentPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findCurrentCompareInstantaneous = function findCurrentCompareInstantaneous({ 
  dataSetName, elementsId, limit, metric = 'average_phase_current',rangeOne, rangeTwo }) {
    const tableName = 'electrical_measurements'
    const query = `
    SELECT DISTINCT 
        elementId, 
        DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime,
        electricalCurrent,
        currentPhase1,
        currentPhase2,
        currentPhase3, 
        '${metric}' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementsId}'
    AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
    OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
     ORDER BY
     loadStoringTime 
   ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

  /**
 * This query gets powerFactor per Instantaneous between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   powerFactor,
 *   powerFactorPhase1,
 *   powerFactorPhase2,
 *   powerFactorPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findPowerFactorComparePerHourAndDayAndWeekAndMonthAndYear = 
 function findPowerFactorComparePerHourAndDayAndWeekAndMonthAndYear({ 
  dataSetName, elementsId, granularity, limit, metric = 'average_phase_current',rangeOne, rangeTwo }) {
    const tableName = 'electrical_measurements'
    const formatDate = getFormatDateSqlByGranularity(granularity)
    const query = `
    SELECT elementId,
        AVG(totalPowerFactor) AS powerFactor,
        MAX(totalPowerFactor) AS powerFactorMax,
        MIN(totalPowerFactor) AS powerFactorMin,
        AVG(totalPowerFactorPhase1) AS powerFactorPhase1,
        MAX(totalPowerFactorPhase1) AS powerFactorPhase1Max,
        MIN(totalPowerFactorPhase1) AS powerFactorPhase1Min,
        AVG(totalPowerFactorPhase2) AS powerFactorPhase2,
        MAX(totalPowerFactorPhase2) AS powerFactorPhase2Max,
        MIN(totalPowerFactorPhase2) AS powerFactorPhase2Min,
        AVG(totalPowerFactorPhase3) AS powerFactorPhase3,
        MAX(totalPowerFactorPhase3) AS powerFactorPhase3Max,
        MIN(totalPowerFactorPhase3) AS powerFactorPhase3Min,
        FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
        '${metric}' AS metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementsId}' 
      AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
      OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
    GROUP BY
      elementId,
      loadStoringTime
     ORDER BY
      loadStoringTime DESC 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

/**
 * This query gets powerFactor per Instantaneous between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   powerFactor,
 *   powerFactorPhase1,
 *   powerFactorPhase2,
 *   powerFactorPhase3,
 * },
 * .....
 * ]
 */
 module.exports.findPowerFactorCompareInstantaneous = function findPowerFactorCompareInstantaneous({ 
  dataSetName, elementsId, limit, metric = 'average_phase_powerFactor',rangeOne, rangeTwo }) {
    const tableName = 'electrical_measurements'
    const query = `
    SELECT DISTINCT 
        elementId, 
        DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime,
        totalPowerFactor powerFactor,
        totalPowerFactorPhase1 powerFactor1,
        totalPowerFactorPhase2 powerFactor2,
        totalPowerFactorPhase3 powerFactor3, 
        '${metric}' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementsId}'
    AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
    OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
     ORDER BY
     loadStoringTime 
   ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }



  /**
    * Gets the goals monthly by structureId goal and dates
    * @param {string} dataSetName: Name of dataSet 
    * @param {string} structureIds: Id of structure
    * @param {object} goalId: Goal id
    * @param {object} dates: Date
    * @returns {Array} Rows with this goals: 
   */
   module.exports.getGoalMonthly = function getGoalMonthly({ dataSetName, structureIds, goalId, dates }) {
    const tableName = 'goals_monthly'
    const query = `
    SELECT 
      locationId,
      goalId,
      year,
      month,
      value
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE goalId = '${goalId}'
     AND  locationId IN (${joinItemsWithCommaForQuery(structureIds)}) 
     AND (${getDateArrayInMonthAndYear({ dates })})
     ORDER BY locationId, goalId, year, month  
    `
  
    return runQuery(query)
  }

 /**
  * Gets the last value and connection
  * 
  * @param {string} dataSetName: Name of the company
  * @param {array} elementIds: Array of elements id
  *
  * @returns {array} Last rows
  */
  module.exports.findLastConnectionValueEnergy = function findLastConnectionValueEnergy({ dataSetName, elementsId }) {
    const tableName = 'warehouse'
    const query = `
    SELECT
     elementId,
     lastRow.consumption lastValue,
     lastRow.loadStoringTime lastConnection
    FROM (
      SELECT
        elementId,
        ARRAY_AGG(STRUCT( consumption, loadStoringTime)
        ORDER BY
        loadStoringTime DESC
        LIMIT 1)[OFFSET(0)] lastRow
      FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE
     elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    GROUP BY 1 )
    `
    return runQuery(query)
  }