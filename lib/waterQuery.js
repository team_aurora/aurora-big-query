const { getElementsIdForQuery, getFrom, getProjectId, runQuery } = require('./bigQuery')
const { joinItemsWithCommaForQuery } =  require('aurora-bigquery/lib/helpers/dataHelpers')

module.exports.findWaterConsumption = function findWaterConsumption({ dataSetName, tableName = 'water_consumption', accumulatedData, date, elements, location }) {
  const elementsId = accumulatedData && accumulatedData.length ? accumulatedData[0].elements.map((item) => item.elementId) : elements.map((item) => item.elementId)
  const query = `
  SELECT loadStoringTime,
      'gasConsumption' AS metric,
      consumption,
      '${location}' AS location,
      elementId
      FROM  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
      WHERE 
      consumption IS NOT NULL
      AND elementId IN (${getElementsIdForQuery(elementsId)})
      AND (DATETIME(loadStoringTime) BETWEEN DATETIME('${date[0] + ' 00:00:00'}')
      AND DATE_ADD(DATETIME('${date[1] + ' 00:00:00'}'), INTERVAL 1 DAY))
      ORDER BY
      loadStoringTime ASC
  `
  return runQuery(query)
}

module.exports.findMaxCounterWaterConsumption = function findMaxWaterGasConsumption({
  dataSetName,
  tableName,
  elementsId
}) {
  const query = `SELECT elementId, ARRAY_AGG(STRUCT(loadStoringTime, fCnt)
  ORDER BY loadStoringTime DESC LIMIT 1) consumption 
  FROM  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})  GROUP BY 1`

  return runQuery(query)
}

module.exports.findMaxLoadStoringTimeWaterEvent = function findMaxLoadStoringTimeWaterEvent({
  dataSetName,
  tableName,
  elementsId
}) {
  const query = `SELECT 
  max(loadStoringTime), elementId FROM 
    \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    group by elementId`

  return runQuery(query)
}

module.exports.findLastConsumptionWater = function findLastConsumptionWater({
  dataSetName,
  tableName,
  elementsId
}) {
  const query = `SELECT elementId, ARRAY_AGG(STRUCT(createdAt, consumption) ORDER BY createdAt DESC LIMIT 1) 
  consumption FROM  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\` 
  WHERE elementId IN (${getElementsIdForQuery(elementsId)}) GROUP BY 1`

  return runQuery(query)
}

module.exports.findLastLowPower = function findLastLowPower({
  dataSetName,
  tableName,
  listCodesReasonLowBattery,
  elementsId
}) {
  const query = `SELECT elementId, ARRAY_AGG(STRUCT(createdAt, meterStatus) ORDER BY createdAt DESC LIMIT 1)  
  FROM  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE meterStatus IN (${getElementsIdForQuery(listCodesReasonLowBattery)})
  AND elementId IN (${getElementsIdForQuery(elementsId)})
  GROUP BY 1`

  return runQuery(query)
}

/**
  * Gets the last value and connection
  * 
  * @param {string} dataSetName: Name of the company
  * @param {array} elementIds: Array of elements id
  *
  * @returns {array} Last rows
  */
 module.exports.findLastConnectionValueWater = function findLastConnectionValueWater({ dataSetName, elementsId }) {
  const tableName = 'water_consumption'
  const query = `
  SELECT
   elementId,
   lastRow.consumption lastValue,
   lastRow.loadStoringTime lastConnection
  FROM (
    SELECT
      elementId,
      ARRAY_AGG(STRUCT( consumption, loadStoringTime)
      ORDER BY
      loadStoringTime DESC
      LIMIT 1)[OFFSET(0)] lastRow
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE
   elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  GROUP BY 1 )
  `
  return runQuery(query)
}