const _ = require('lodash');
const q = require('q');
const async = require('async')
const { BigQuery } = require('@google-cloud/bigquery');
const moment = require('moment');

// Helpers
const { joinItemsWithCommaForQuery } = require('./helpers/dataHelpers')
const { getAllWeeks, getAllYears, getDateArrayInSqlFormat, getDateArrayInMonthAndYear } =  require('./helpers/granularityHelpers')

// Variables
let config;
let projectId;
let bigQuery;


function getDaysOfWeekForBigquery(dayOfWeek) {
  if (!dayOfWeek) {
    return null
  }
  if (dayOfWeek === 7) {
    return 1
  }
  return dayOfWeek + 1
}


function getDateString({ loadStoringTime, year, month, week }) {
  let dateString
  let formatDate
  let endOf
  if (loadStoringTime) {
    dateString = loadStoringTime.value || loadStoringTime
    formatDate = 'YYYY-MM-DDTHH:mm:ss'
    endOf = 'day'
  } else if (year && week) {
    dateString = `${year}-${week}`
    formatDate = 'GGGG-W'
    endOf = 'isoWeek'
  } else if (year && month) {
    dateString = `${year}-${month}`
    formatDate = 'Y-M'
    endOf = 'month'
  } else {
    dateString = `${year}`
    formatDate = 'Y'
    endOf = 'year'
  }
  return {
    dateString,
    formatDate,
    endOf
  }
}

function uniqByElementIdAndLoadStoringTime(data) {
  return _.uniq(data, d => {
    const { dateString } = getDateString(d)
    return [dateString, d.elementId || d.location].join()
  })
}

function orderByLoadStoringTime(data) {
  return data.sort((a, b) => {
    const one = getDateString(a)
    const two = getDateString(b)
    return moment(one.dateString, one.formatDate).toDate() - moment(two.dateString, two.formatDate).toDate()
  })
}


function getElementsIdForQuery(elementsId) {
  const total = elementsId.length - 1
  return elementsId.reduce((result, elementId, index) => {
    result = result + `'${elementId}'`
    if (index !== total) {
      result = result + ','
    }
    return result
  }, '')
}
module.exports.getElementsIdForQuery = getElementsIdForQuery

function getQueryForDatesByElements({ datesByElementId }) {
  const formatDate = 'YYYY-MM-DD'
  const elementsId = Object.keys(datesByElementId)
  const length = elementsId.length - 1
  return elementsId
    .reduce((result, elementId, index) => {
      const dates = datesByElementId[elementId].dates
      result += `(elementId = '${elementId}' AND DATE(loadStoringTime) BETWEEN DATE('${dates[0].format(formatDate)}') AND DATE('${dates[1].format(formatDate)}'))`
      if (index < length) {
        result += ' OR '
      }
      return result
    }, '')
}

function getFrom({ projectId, dataSetName, tableName }) {
  return `${projectId}.${dataSetName}.${tableName}`
}
module.exports.getFrom = getFrom

function buildQueryWeeks({ weekStart, weekEnd, year }) {
  return `(week BETWEEN ${weekStart}
  AND ${weekEnd}
  AND year = ${year})`
}

module.exports.init = function init(configReq) {
  config = configReq;
  projectId = config.gcloud.projectId;

  // Instantiates a client
  bigQuery = new BigQuery({
    projectId: projectId,
    credentials: config.gcloud.bigQueryCredentials
  });
}

module.exports.getSchema = function getSchema() {
  return "env:string, elementId:string, loadStoringTime:date, totalActiveEnergy:float, totalRectiveEnergy:float, frequency:float, power:float, powerFactor:float, voltage:float, voltagePhase1:float, voltagePhase2:float, voltagePhase3:float, currentPhase1:float, currentPhase2:float, currentPhase3:float, instantaneousActivePower:float, activePowerPhase1:float, activePowerPhase2:float, activePowerPhase3:float, instantaneousReactivePower:float, reactivePowerPhase1:float, reactivePowerPhase2:float, reactivePowerPhase3:float, totalPowerFactor:float, totalPowerFactorPhase1:float, totalPowerFactorPhase2:float, totalPowerFactorPhase3:float, activeDemand:float, frame:string, startReceiveTime:date, createdAt:date, identificationCode:string, relayStatus:string, lastGasp:string, consumption:float, reactiveConsumption:float, temperature:float, doorStatus:float"
}

module.exports.createDataSet = function createDataSet(name) {
  return new Promise((resolve, reject) => {
    const dataset = bigQuery.dataset(name);
    dataset.exists((err, exists) => {
      if (err) {
        reject(err);
      } else if (exists) {
        resolve(exists);
      } else {
        bigQuery.createDataset(name)
          .then((results) => {
            const dataset = results[0];
            console.log(`Dataset ${dataset.id} created.`);
            resolve(dataset);
          })
          .catch((err) => {
            console.error('ERROR:', err);
            reject(err);
          });
      }
    });
  });
}

/**
 * @serviceName createTable
 * @apiDescription create table in BigQuery
 *
 * @param {String} dataSetName BigQuery Dataset name
 * @param {String} tableName table name
 * @param {String} schema Table schema
 *
 * "elementId:string, activeEnergy:float, activeEnergyP2:float, activeEnergyP3:float, voltage:float, voltageP1:float, voltageP2:float"
 *
 * @param {Bool} currentSchema Flag to know if the the method is getting the schema or use the default one
 *
 */

module.exports.createTable = function createTable(dataSetName, tableName, schema, currentSchema) {

  return new Promise((resolve, reject) => {
    const options = (currentSchema) ? { schema: this.getSchema() } : { schema: schema };
    bigQuery
      .dataset(dataSetName)
      .createTable(tableName, options)
      .then((results) => {
        const table = results[0];
        console.log(`Table ${table.id} created.`);
        resolve(table)
      })
      .catch((err) => {
        console.error('ERROR:', err);
        reject(err)
      });
  });
}

/**
 * @serviceName insertRecord
 * @apiDescription Insert new record in BigQuery
 *
 * @param {String} dataSetName BigQuery Dataset name
 * @param {String} tableName table name
 * @param {Array or Object} data Object/s to save
 *
 *   [{
 *     elementId:1,
 *     activeEnergy:10,
 *     voltage:5
 *   },
 *   {
 *     elementId:2,
 *     activeEnergy:10,
 *     activeEnergyP2:5,
 *     voltage:5
 *   },
 *   {
 *     elementId:2,
 *     activeEnergy:10,
 *     activeEnergyP2:5,
 *     activeEnergyP3:3,
 *     voltage:5
 *   }]
 *
 */

module.exports.insertRecord = function insertRecord(dataSetName, tableName, data) {
  // console.log("table and set to bigquery " + dataSetName + " " + tableName);
  // console.log("insert into bigquery" + JSON.stringify(data));
  const dataset = bigQuery.dataset(dataSetName);
  const table = dataset.table(tableName);
  return table.insert(data)
    .then((results) => results[0])
    .catch((err) => {
      // console.log("insert into bigquery error");
      // console.log(JSON.stringify(err));
      return err;
    });
}
/**
 * @serviceName generateQuery
 * @apiDescription generate SQL query ofr BigQuert
 *
 * @param {Strinf or Array} fields Fields to bring in the query
 *  * or ['field1', 'field2']
 *
 * @param {String} dataSetName BigQuery Dataset name
 * @param {String} tableName table name
 * @param {Array} conditions array with the conditions if any
 *  [ 
 +   {condition:[ 'name', 'algo' ] },
 *   {condition:[ 'date', '<' '201787678' ] },
 *   {condition:[ 'ids', 'in' ['1','2','3] ] }
 *  ]  
 * @param {Int} limit the limit of the records do you want to bring
 *
 */
module.exports.generateQuery = function generateQuery(fields, dataSetName, tableName, conditions, groupBy, orderBy, limit) {
  return new Promise((resolve, reject) => {
    let str = 'SELECT ' + (_.isArray(fields) ? fields.join(',') : fields);
    str += ' FROM ';
    str += `\`${getFrom({ projectId, dataSetName, tableName })}\``

    if (conditions && _.isArray(conditions) && conditions.length > 0) {
      var conditionsList = []
      async.eachSeries(conditions, function (condition, condictionCb) {
        const field = condition.field;
        const operator = condition.operator;
        let value = condition.value;
        const skipOperator = !!condition.skip;

        if (!_.isEmpty(operator)) {
          if (_.isArray(value)) {
            var newValue = '(';
            var newValueArray = []
            if (condition.noString) {
              _.each(value, function (v) {
                newValueArray.push(v);
              })
            } else {
              _.each(value, function (v) {
                newValueArray.push("'" + v + "'");
              })
            }
            newValue += newValueArray.join(",")
            newValue += ')';
            value = newValue
          }
          var conditionStr = conditions.length > 1 ? `(${field} ${operator} ${value})` : `${field} ${operator} ${value}`
          conditionsList.push(conditionStr)
        } else if (skipOperator) {
          var conditionStr = `( ${field} ${value} )`;
          conditionsList.push(conditionStr)
        } else {
          var conditionStr = conditions.length > 1 ? `(${field} = ${value})` : `${field} = ${value}`
          conditionsList.push(conditionStr)
        }

        condictionCb()

      }, function (err) {
        str += ' WHERE ' + conditionsList.join(' AND ');
        if (groupBy) {
          str += ' GROUP BY ' + (_.isArray(groupBy) ? groupBy.join(",") : groupBy)
        }
        if (orderBy) {
          str += ' ORDER BY ' + (_.isArray(orderBy) ? orderBy.join(",") : orderBy);
        }
        if (limit) {
          str += ' LIMIT ' + (limit || 100);
        }
        resolve(str)
      });

    } else if (_.isObject(conditions)) {

      const field = conditions.field;
      const operator = conditions.operator;
      const value = conditions.value;
      str += `${field} ${operator} ${value}`;
      if (groupBy) {
        str += ' GROUP BY ' + (_.isArray(groupBy) ? groupBy.join(",") : groupBy)
      }
      if (orderBy) {
        str += ' ORDER BY ' + (_.isArray(orderBy) ? orderBy.join(",") : orderBy);
      }
      str += ' LIMIT ' + (limit || 100);
      resolve(str)
    }
  });
}

/**
 * @serviceName runQuery
 * @apiDescription Run SQL query
 *
 * @param {String} queryString SQL String
 *
 */

function runQuery(queryString) {
  return bigQuery.query(queryString)
    .then((result) => {
      result = result[0];
      return result;
    })
}
module.exports.runQuery = runQuery

/*
module.exports.runNoLegacyQuery = function(queryString){
  var defer = q.defer();

  const options = {
    query: queryString,
    useLegacySql: false, 
  };

  bigQuery
  .createQueryJob(options, function(err, job) {
    if (err) {
      defer.reject(err);
    }

    job.getQueryResults(function(err, rows) {
      if(err)
        defer.reject(err);
      else
        defer.resolve(rows);
    });
  });


  return defer.promise;
}*/

module.exports.insertLog = function create(data) {
  data.createdAt = moment().format('YYYY-MM-DD HH:mm:ss');
  data.env = process.env.NAMESPACE || 'dev';
  return this.insertRecord(config.logs.dataset, 'warehouse', data)
    .then((result) => result)
    .catch((err) => {
      return ({ userMessage: 'BigQuery Insert Error', data: err });
    });
}

/*
function remove(where){
  var defer = q.defer();

  var query = "DELETE FROM ";
  query += `${config.logs.dataset}.warehouse `;
  query += 'WHERE '+where;

  console.log("DELETE QUERY :: ",query);

  bigQueryService.runNoLegacyQuery(query)
  .then((result) => {
    defer.resolve(result);
  }, (err) => {
    defer.reject({ userMessage: 'BigQuery Remove Error', data: err });
  })
  .catch((err) => {
    defer.reject({ userMessage: 'BigQuery Remove Error', data: err });
  });

  return defer.promise;
}*/

function getAccumulatedMethod({ method }) {
  switch (method) {
    case 'consumptionPerHour':
      return buildQueryAccumulatedConsumptionPerHour
    case 'loadProfilePerHour':
      return buildQueryAccumulatedLoadProfilePerHour
    case 'consumptionPerDay':
      return buildQueryAccumulatedConsumptionPerDay
    case 'loadProfilePerDay':
      return buildQueryAccumulatedLoadProfilePerDay
    case 'consumptionPerWeek':
      return buildQueryAccumulatedConsumptionPerWeek
    case 'loadProfilePerWeek':
      return buildQueryAccumulatedLoadProfilePerWeek
    case 'consumptionPerMonth':
      return buildQueryAccumulatedConsumptionPerMonth
    case 'loadProfilePerMonth':
      return buildQueryAccumulatedLoadProfilePerMonth
    case 'consumptionPerYear':
      return buildQueryAccumulatedConsumptionPerYear
    case 'loadProfilePerYear':
      return buildQueryAccumulatedLoadProfilePerYear
  }
}

module.exports.findAccumulated = function findAccumulated({ dataSetName, accumulatedData, date, method, limit, isReactive, orderBy }) {
  const buildQuery = getAccumulatedMethod({ method })
  const length = accumulatedData.length
  let query = accumulatedData.reduce((results, data, key) => {
    let elementsId = data.elements.map(element => {
      return element.elementId
    })
    if (elementsId.length === 0) {
      elementsId.push('')
    }
    if (length > 1) {
      results = `${results} (${buildQuery({ dataSetName, elementsId, date, accumulatedName: data.name, limit, isReactive })})`
      if (key + 1 < length) {
        results = `${results} UNION ALL `
      }
    } else {
      results = buildQuery({ dataSetName, elementsId, date, accumulatedName: data.name, limit, isReactive })
    }
    return results
  }, '')

  if (orderBy) {
    query = `${query} order by ${orderBy}`
  }

  if (limit > 0) {
    query = `${query} LIMIT ${limit}`
  }
  return runQuery(query).then((data) => {
    return orderByLoadStoringTime(data)
  }).catch((err) => {
    console.error(err)
    return []
  })
}

function getCompareMethod({ method }) {
  switch (method) {
    case 'consumptionPerHour':
      return buildQueryAccumulatedCompareConsumptionPerHour
    case 'consumptionPerDay':
      return buildQueryAccumulatedCompareConsumptionPerDay
    case 'loadProfilePerDay':
      return buildQueryAccumulatedCompareLoadProfilePerDay
    case 'consumptionPerWeek':
      return buildQueryAccumulatedCompareConsumptionPerWeek
    case 'loadProfilePerWeek':
      return buildQueryAccumulatedCompareLoadProfilePerWeek
    case 'consumptionPerMonth':
      return buildQueryAccumulatedCompareConsumptionPerMonth
    case 'loadProfilePerMonth':
      return buildQueryAccumulatedCompareLoadProfilePerMonth
    case 'consumptionPerYear':
      return buildQueryAccumulatedCompareConsumptionPerYear
    case 'loadProfilePerYear':
      return buildQueryAccumulatedCompareLoadProfilePerYear
      case 'indicatorPerMonth':
        return buildQueryAccumulatedCompareIndicatorPerMonth
  }
}

module.exports.findAccumulatedCompare = function findAccumulatedCompare({ dataSetName, accumulatedData, rangeOne, rangeTwo, method, indicatorId }) {
  const buildQuery = getCompareMethod({ method })
  const length = accumulatedData.length
  const query = accumulatedData.reduce((results, data, key) => {
    const elementsId = data.elements.map(element => {
      return element.elementId
    })
    if (length > 1) {
      results = `${results} (${buildQuery({ dataSetName, elementsId, locationId: [data.id], indicatorId, rangeOne, rangeTwo, accumulatedName: data.name })})`
      if (key + 1 < length) {
        results = `${results} UNION ALL `
      }
    } else {
      results = buildQuery({ dataSetName, elementsId,  locationId: [data.id], indicatorId, rangeOne, rangeTwo, accumulatedName: data.name })
    }
    return results
  }, '')
  return runQuery(query)
    .then(data => { // PORQUE CUANDO SE COMPRARA UN DIA CONSECUTIVO A OTRO EL QUERY LOS TRAE DUPLICADOS
      return uniqByElementIdAndLoadStoringTime(data)
    })
}

// CONSUMPTION - HELPERS

module.exports.getLastConsumptionValue = function getLastConsumptionValue({ dataSetName, elementsId }) {
  const tableName = 'warehouse'
  const from = getFrom({ projectId, dataSetName, tableName })
  const query = `
  SELECT  t.elementId, t.consumption, t.loadStoringTime
  FROM 
    \`${from}\` as t,
    (
      SELECT
      elementId, MAX(loadStoringTime) AS loadStoringTime
      FROM
      \`${from}\`
      WHERE
        elementId IN (${getElementsIdForQuery(elementsId)})
      GROUP BY 
      elementId
    ) as lastValues
  WHERE
    t.elementId = lastValues.elementId AND t.loadStoringTime = lastValues.loadStoringTime 
  `
  return runQuery(query)
}

module.exports.getLastConsumptionProfileValue = async function getLastConsumptionProfileValue({ dataSetName, elementsId }) {
  const tableName = 'consumption_profile_hour'
  const from = getFrom({ projectId, dataSetName, tableName })
  const query = `
  SELECT  t.elementId, t.consumptionProfile, t.loadStoringTime
  FROM 
    \`${from}\` as t,
    (
      SELECT
      elementId, MAX(loadStoringTime) AS loadStoringTime
      FROM
      \`${from}\`
      WHERE
        elementId IN (${getElementsIdForQuery(elementsId)})
      GROUP BY 
      elementId
    ) as lastValues
  WHERE
    t.elementId = lastValues.elementId AND t.loadStoringTime = lastValues.loadStoringTime 
  `
  try {
    const result = await runQuery(query)
    return result
  } catch (error) {
    console.error(error)
    return []
  }
}

// Revolution - HELPERS

module.exports.getLastRevolutionValue = function getLastRevolutionValue({ dataSetName, elementsId }) {
  const tableName = 'warehouse'
  const from = getFrom({ projectId, dataSetName, tableName })
  const query = `
  SELECT  t.elementId, t.revolution, t.loadStoringTime
  FROM 
    \`${from}\` as t,
    (
      SELECT
      elementId, MAX(loadStoringTime) AS loadStoringTime
      FROM
      \`${from}\`
      WHERE
        elementId IN (${getElementsIdForQuery(elementsId)})
        AND revolution IS NOT NULL
      GROUP BY 
      elementId
    ) as lastValues
  WHERE
    t.elementId = lastValues.elementId AND t.loadStoringTime = lastValues.loadStoringTime AND revolution IS NOT NULL
  `
  return runQuery(query)
}

module.exports.getLastDayOfConsumption = function getLastDayOfConsumption({ dataSetName, elementsId }) {
  const tableName = 'warehouse'
  const from = getFrom({ projectId, dataSetName, tableName })
  const query = `
  SELECT
    t.elementId,
    t.consumption,
    t.loadStoringTime
  FROM
    \`${from}\` as t,
    (
    SELECT
      elementId,
      MAX(loadStoringTime) AS loadStoringTime
    FROM
      \`${from}\`
    WHERE
      elementId IN (${getElementsIdForQuery(elementsId)})
    GROUP BY
      elementId ) AS lastValues
  WHERE
    t.elementId = lastValues.elementId
  GROUP BY
    t.elementId,
    t.consumption,
    t.loadStoringTime,
    lastValues.loadStoringTime
  HAVING
    DATE(t.loadStoringTime) = DATE(lastValues.loadStoringTime)
  ORDER BY
    t.loadStoringTime
  `
  return runQuery(query)
}

// CONSUMPTION - HOUR

module.exports.findConsumptionPerHourAndElementId = function findConsumptionPerHourAndElementId({ dataSetName, datesByElementId, isReactive }) {
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const tableName = 'warehouse'
  const query = `
  SELECT 
    DISTINCT loadStoringTime,
    elementId,
    '${field}' AS metric,
    ${field}
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE (${getQueryForDatesByElements({ datesByElementId })})
  ORDER BY
    loadStoringTime ASC`
  return runQuery(query)
}

module.exports.findConsumptionProfilePerHourAndElementId = function findConsumptionProfilePerHourAndElementId({ dataSetName, datesByElementId, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile	'
  const tableName = 'consumption_profile_hour'
  const query = `
  SELECT 
    DISTINCT loadStoringTime,
    elementId,
    '${field}' AS metric,
    ${field}
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE (${getQueryForDatesByElements({ datesByElementId })})
  ORDER BY
    loadStoringTime ASC`
  return runQuery(query)
}

function buildQueryConsumptionPerHourNextDay({ dataSetName, elementsId, date, field = 'consumption' }) {
  const tableName = 'warehouse'
  const nextDate = moment(date[1]).add(1, 'd').format('YYYY-MM-DD')
  const query = `SELECT 
    DISTINCT loadStoringTime,
    elementId,
    '${field}' AS metric,
    ${field}
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE ${field} IS NOT NULL
  AND elementId IN (${getElementsIdForQuery(elementsId)})
  AND DATE(loadStoringTime) = DATE('${nextDate}')
  ORDER BY
    loadStoringTime ASC
  LIMIT 1`
  return query
}

module.exports.findConsumptionPerHour = function findConsumptionPerHour({ dataSetName, elementsId, date, dayOfWeek, limit, isReactive }) {
  const tableName = 'warehouse'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD')
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query = `
  SELECT 
    DISTINCT loadStoringTime,
    elementId,
    '${field}' AS metric,
    ${field}
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND (identificationCode != '9010' OR identificationCode IS NULL)
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    AND ${field} IS NOT NULL
    ${(dayOfWeek && dayOfWeek > 0) ? `AND (EXTRACT(DAYOFWEEK FROM loadStoringTime) = ${dayOfWeek} OR EXTRACT(DAYOFWEEK FROM loadStoringTime) = ${getDaysOfWeekForBigquery(dayOfWeek)} OR EXTRACT(DAYOFWEEK FROM loadStoringTime) = ${(dayOfWeek > 5) ? dayOfWeek - 5 : dayOfWeek + 2})` : ''}
  ORDER BY
    loadStoringTime ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).then((data) => {
    return orderByLoadStoringTime(data)
  })
}

module.exports.findConsumptionProfilePerHour = function findConsumptionProfilePerHour({ dataSetName, elementsId, date, dayOfWeek, limit, isReactive }) {
  const tableName = 'consumption_profile_hour'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD')
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const query = `
  SELECT 
    DISTINCT loadStoringTime,
    elementId,
    '${field}' AS metric,
    ${field}
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    AND ${field} IS NOT NULL
    ${(dayOfWeek && dayOfWeek > 0) ? `AND (EXTRACT(DAYOFWEEK FROM loadStoringTime) = ${dayOfWeek} OR EXTRACT(DAYOFWEEK FROM loadStoringTime) = ${getDaysOfWeekForBigquery(dayOfWeek)} OR EXTRACT(DAYOFWEEK FROM loadStoringTime) = ${(dayOfWeek > 5) ? dayOfWeek - 5 : dayOfWeek + 2})` : ''}
  ORDER BY
    loadStoringTime ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).then((data) => {
    return orderByLoadStoringTime(data)
  })
}

function buildQueryAccumulatedConsumptionPerHourNextDay({ dataSetName, elementsId, accumulatedName, date, field = 'consumption' }) {
  const tableName = 'warehouse'
  const nextDate = moment(date[1]).add(1, 'd').format('YYYY-MM-DD')
  const query = `
  SELECT
    DISTINCT t.loadStoringTime,
    '${field}' AS metric,
    t.${field} as total,
    '${accumulatedName}' AS location,
    t.elementId
  FROM (
    SELECT
      MIN(loadStoringTime) AS loadStoringTime,
      elementId
    FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE
      elementId IN (${getElementsIdForQuery(elementsId)})
      AND DATE(loadStoringTime) = DATE('${nextDate}')
    GROUP BY
      elementId) AS m,
    \`${getFrom({ projectId, dataSetName, tableName })}\` AS t
   WHERE
    t.${field} IS NOT NULL
    AND t.elementId = m.elementId
    AND t.loadStoringTime = m.loadStoringTime`
  return query
}

function getAllQuerysAccumulatedNextDayByElements({ dataSetName, elementsId, accumulatedName, date, method, field }) {
  const buildQuery = getMethodByHour({ method })
  const query = buildQuery({ dataSetName, elementsId, date, accumulatedName, field })
  return query
}

function buildQueryAccumulatedConsumptionPerHour({ dataSetName, elementsId, accumulatedName, date, limit, isReactive }) {
  const tableName = 'warehouse'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const query =
    `(
      SELECT
      DISTINCT loadStoringTime,
      '${field}' AS metric,
      ${field} as total,
      '${accumulatedName}' AS location,
      elementId
    FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE
      ${field} IS NOT NULL
      AND (identificationCode != '9010' OR identificationCode IS NULL)
      AND elementId IN (${getElementsIdForQuery(elementsId)})
      AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}')
        AND DATE('${endDate}'))
    ORDER BY
      loadStoringTime ASC
    ${limit ? `LIMIT ${limit}` : ''})
    UNION ALL
  `
  return `${query} (${getAllQuerysAccumulatedNextDayByElements({ dataSetName, elementsId, accumulatedName, date, method: 'consumptionAccumulatedPerHour', field })})`
}

module.exports.findConsumptionComparePerHour = function findConsumptionComparePerHour({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'warehouse'
  const query = `
  (SELECT
    DISTINCT loadStoringTime,
    elementId,
    'consumption' AS metric,
    consumption
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE consumption IS NOT NULL
  AND elementId = '${elementId}'
  AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
  ORDER BY
    loadStoringTime)
  UNION ALL ${getAllQuerysCompareNextDayByElements({ dataSetName, elementsId: [elementId], rangeOne, rangeTwo, method: 'consumptionComparePerHour' })}`

  return runQuery(query).then(data => {
    return uniqByElementIdAndLoadStoringTime(data)
  })
}

function getMethodByHour({ method }) {
  switch (method) {
    case 'consumptionPerHour':
      return buildQueryConsumptionPerHourNextDay
    case 'consumptionComparePerHour':
      return buildQueryConsumptionPerHourNextDay
    case 'consumptionAccumulatedPerHour':
      return buildQueryAccumulatedConsumptionPerHourNextDay
    case 'consumptionAccumulatedComparePerHour':
      return buildQueryAccumulatedConsumptionPerHourNextDay
  }
}

function getAllQuerysCompareNextDayByElements({ dataSetName, elementsId, rangeOne, rangeTwo, method }) {
  const buildQuery = getMethodByHour({ method })
  const length = elementsId.length
  const query = elementsId.reduce((results, elementId, key) => {
    if (length > 1) {
      results = `${results} (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeOne })})`
      results = `${results} (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeTwo })})`
      if (key + 1 < length) {
        results = `${results} UNION ALL `
      }
    } else {
      results = `${results} (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeOne })})`
      results = `${results} UNION ALL (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeTwo })})`
    }
    return results
  }, '')
  return query
}

// function getAllQuerysNextDayByElements({ dataSetName, elementsId, date, method, field }) {
//   const buildQuery = getMethodByHour({ method })
//   const length = elementsId.length
//   const query = elementsId.reduce((results, elementId, key) => {
//     if (length > 1) {
//       results = `${results} (${buildQuery({ dataSetName, elementsId: [elementId], date, field })})`
//       if (key + 1 < length) {
//         results = `${results} UNION ALL `
//       }
//     } else {
//       results = buildQuery({ dataSetName, elementsId, date, field })
//     }
//     return results
//   }, '')
//   return query
// }

function getAllQuerysAccumulatedCompareNextDayByElements({ dataSetName, elementsId, accumulatedName, rangeOne, rangeTwo, method }) {
  const buildQuery = getMethodByHour({ method })
  const length = elementsId.length
  const query = elementsId.reduce((results, elementId, key) => {
    if (length > 1) {
      results = `${results} (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeOne, accumulatedName })})`
      results = `${results} UNION ALL (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeTwo, accumulatedName })})`
      if (key + 1 < length) {
        results = `${results} UNION ALL `
      }
    } else {
      results = `${results} (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeOne, accumulatedName })})`
      results = `${results} UNION ALL (${buildQuery({ dataSetName, elementsId: [elementId], date: rangeTwo, accumulatedName })})`
    }
    return results
  }, '')
  return query
}

function buildQueryAccumulatedCompareConsumptionPerHour({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'warehouse'
  const query =
    `(
      SELECT
        loadStoringTime,
        'consumption' AS metric,
        consumption AS total,
        '${accumulatedName}' AS location,
        elementId
      FROM (
        SELECT 
          DISTINCT loadStoringTime,
          elementId,
          consumption
        FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
        WHERE 
          consumption IS NOT NULL
          AND elementId IN (${getElementsIdForQuery(elementsId)})
          AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
          OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
      ))
  UNION ALL`

  return `${query} ${getAllQuerysAccumulatedCompareNextDayByElements({ dataSetName, elementsId, accumulatedName, rangeOne, rangeTwo, method: 'consumptionAccumulatedComparePerHour' })}`
}

module.exports.findConsumptionComparePerDay = function findConsumptionComparePerDay({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_daily'
  const query = `SELECT *, 'consumption' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((loadStoringTime BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (loadStoringTime BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
   ORDER BY
     year ASC,
     month ASC,
     day ASC`

  return runQuery(query)
}

function buildQueryAccumulatedConsumptionPerDay({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_daily'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query =
    `SELECT 
    day,
    month,
    year,
    loadStoringTime,
    '${field}' AS metric,
    SUM(${field}) AS total,
    '${accumulatedName}' AS location,
    SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  GROUP BY
    day,
    month,
    year,
    loadStoringTime
  ORDER BY
    loadStoringTime ASC`
  return query
}

function buildQueryAccumulatedCompareConsumptionPerDay({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_daily'
  const query =
    `SELECT 
    day,
    month,
    year,
    loadStoringTime,
    'consumption' AS metric,
    SUM(consumption) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ((loadStoringTime BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (loadStoringTime BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
  GROUP BY
    loadStoringTime,
    day,
    month,
    year
  ORDER BY
    loadStoringTime ASC`
  return query
}

module.exports.averageConsumptionProfile = function averageConsumptionProfile({ dataSetName, locationId, elementsId, date, groupBy }) {
  if (!elementsId || !elementsId.length) {
    return Promise.resolve([])
  }
  const tableName = 'consumption_profile_daily'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')

  let query = `SELECT elementId, year, avg(consumptionProfile) as avg, EXTRACT(${groupBy} from loadStoringTime) AS ${groupBy}
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}') 
  GROUP BY elementId, year, ${groupBy}
  ORDER BY year ASC`

  if (locationId) {
    query = `SELECT
      '${locationId}' AS locationId,
      EXTRACT(YEAR from loadStoringTime) AS year, 
      AVG(total) AS avg,
      EXTRACT(${groupBy}
      FROM
        loadStoringTime) AS ${groupBy}
    FROM (
      SELECT
        SUM(consumptionProfile) AS total,
        loadStoringTime
      FROM
        \`${getFrom({ projectId, dataSetName, tableName })}\`
      WHERE
        elementId IN (${getElementsIdForQuery(elementsId)})
        AND loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}') 
      GROUP BY
        loadStoringTime)
    GROUP BY year, ${groupBy}
    ORDER BY year ASC`
  }
  return runQuery(query).catch(() => [])
}

function buildQueryAccumulatedLoadProfilePerDay({ dataSetName, elementsId, date, accumulatedName, isReactive }) {
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const tableName = 'consumption_profile_daily'
  const startDate = moment(date[0], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const endDate = moment(date[1], 'YYYY-MM-DD').format('YYYY-MM-DD')
  const query =
    `SELECT 
    day,
    month,
    year,
    loadStoringTime,
    '${field}' AS metric,
    SUM(${field}) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM (SELECT
      DISTINCT loadStoringTime,
      elementId,
      ${field},
      day,
      month,
      year
    FROM
      \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND ${field} IS NOT NULL
    AND (loadStoringTime BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    )
  GROUP BY
    day,
    month,
    year,
    loadStoringTime
  ORDER BY
    loadStoringTime ASC`
  return query
}

module.exports.findLoadProfileComparePerDay = function findLoadProfileComparePerDay({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_profile_daily'
  const query = `SELECT *, 'consumptionProfile' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((loadStoringTime BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (loadStoringTime BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
   ORDER BY
     year ASC,
     month ASC,
     day ASC`

  return runQuery(query)
}

function buildQueryAccumulatedCompareLoadProfilePerDay({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_profile_daily'
  const query =
    `SELECT 
    day,
    month,
    year,
    loadStoringTime,
    'consumptionProfile' as metric,
    SUM(consumptionProfile) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ((loadStoringTime BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (loadStoringTime BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
  GROUP BY
    loadStoringTime,
    day,
    month,
    year
  ORDER BY
    year ASC,
    month ASC,
    day ASC`
  return query
}

function buildQueryAccumulatedConsumptionPerWeek({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_daily'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query =
    `SELECT 
    day,
    month,
    year,
    week,
    loadStoringTime,
    '${field}' AS metric,
    SUM(${field}) AS total,
    '${accumulatedName}' AS location,
    SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllWeeks({ date })})
  AND weekday = 7
  GROUP BY
    day,
    month,
    year,
    week,
    loadStoringTime
  ORDER BY
    year ASC,
    week ASC`
  return query
}

module.exports.findConsumptionComparePerWeek = function findConsumptionComparePerWeek({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_daily'
  const query = `SELECT *, 'consumption' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((${getAllWeeks({ date: rangeOne })})
  OR (${getAllWeeks({ date: rangeTwo })}))
  AND weekday = 7
   ORDER BY
     year ASC,
     month ASC,
     day ASC`

  return runQuery(query)
}

function buildQueryAccumulatedCompareConsumptionPerWeek({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_daily'
  const query =
    `SELECT
    year,
    week,
    loadStoringTime,
    'consumption' AS metric,
    SUM(consumption) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ((${getAllWeeks({ date: rangeOne })})
  OR (${getAllWeeks({ date: rangeTwo })}))
  AND weekday = 7
  GROUP BY
    loadStoringTime,
    week,
    year
  ORDER BY
    loadStoringTime ASC`
  return query
}

function buildQueryAccumulatedLoadProfilePerWeek({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_profile_weekly'
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const query =
    `SELECT
    week,
    year,
    '${field}' AS metric,
    SUM(${field}) AS total,
    '${accumulatedName}' AS location,
    SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllWeeks({ date })})
  GROUP BY
    week,
    year
  ORDER BY
    year ASC,
    week ASC`
  return query
}

module.exports.findLoadProfileComparePerWeek = function findLoadProfileComparePerWeek({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_profile_weekly'
  const query = `SELECT *, 'consumptionProfile' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((${getAllWeeks({ date: rangeOne })})
  OR (${getAllWeeks({ date: rangeTwo })}))
  ORDER BY
   year ASC,
   week ASC`

  return runQuery(query)
}

function buildQueryAccumulatedCompareLoadProfilePerWeek({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_profile_weekly'
  const query =
    `SELECT
    week,
    year,
    'consumptionProfile' AS metric,
    SUM(consumptionProfile) AS total,
    '${accumulatedName}' AS location,
    SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ((${getAllWeeks({ date: rangeOne })})
  OR (${getAllWeeks({ date: rangeTwo })}))
  GROUP BY
    week,
    year
  ORDER BY
    year ASC,
    week ASC`
  return query
}

function buildQueryAccumulatedConsumptionPerMonth({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_daily'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query =
    `SELECT 
    day,
    month,
    year,
    week,
    loadStoringTime,
    '${field}' AS metric,
    SUM(${field}) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND ${field} IS NOT NULL
    AND loadStoringTime IN (${getDateArrayInSqlFormat({ dates: date })})
  GROUP BY
    day,
    month,
    year,
    week,
    loadStoringTime
  ORDER BY
   loadStoringTime`
  return query
}

module.exports.findConsumptionComparePerMonth = function findConsumptionComparePerMonth({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_daily'
  const query = `SELECT *, 'consumption' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ( loadStoringTime IN (${getDateArrayInSqlFormat({ dates: rangeOne })})
  OR loadStoringTime IN (${getDateArrayInSqlFormat({ dates: rangeTwo })}))
   ORDER BY
    loadStoringTime`

  return runQuery(query)
}

function buildQueryAccumulatedCompareConsumptionPerMonth({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_daily'
  const query =
    `SELECT 
    day,
    month,
    year,
    week,
    loadStoringTime,
    'consumption' AS metric,
    SUM(consumption) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND (loadStoringTime IN (${getDateArrayInSqlFormat({ dates: rangeOne })})
      OR loadStoringTime IN (${getDateArrayInSqlFormat({ dates: rangeTwo })})
    )
  GROUP BY
    day,
    month,
    year,
    week,
    loadStoringTime
  ORDER BY
  loadStoringTime`
  return query
}

function buildQueryAccumulatedLoadProfilePerMonth({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_profile_monthly'
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const query =
    `SELECT 
    month,
    year,
    '${field}' AS metric,
    SUM(${field}) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
    AND ${field} IS NOT NULL
    AND (${getDateArrayInMonthAndYear({ dates: date })})
  GROUP BY
    month,
    year
  ORDER BY
   year,
   month`
  return query
}

module.exports.findLoadProfileComparePerMonth = function findLoadProfileComparePerMonth({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_profile_monthly'
  const query = `SELECT *, 'consumptionProfile' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND (${getDateArrayInMonthAndYear({ dates: rangeOne })}
    OR ${getDateArrayInMonthAndYear({ dates: rangeTwo })})
   ORDER BY
    year,
    month`

  return runQuery(query)
}

function buildQueryAccumulatedCompareLoadProfilePerMonth({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_profile_monthly'
  const query =
    `SELECT 
    month,
    year,
    'consumptionProfile' AS metric,
    SUM(consumptionProfile) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND (${getDateArrayInMonthAndYear({ dates: rangeOne })}
    OR ${getDateArrayInMonthAndYear({ dates: rangeTwo })})
  GROUP BY
    month,
    year
  ORDER BY
    year,
    month
    `
  return query
}

// CONSUMPTION YEAR

module.exports.findConsumptionPerYear = function findConsumptionPerYear({ dataSetName, elementsId, date, limit, isReactive }) {
  const tableName = 'consumption_daily'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query =
    `SELECT
    elementId,
    identificationCode,
    consumption,
    year,
    loadStoringTime,
    '${field}' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllYears({ date })})
   ORDER BY
     year ASC
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query).catch(() => [])
}

function buildQueryAccumulatedConsumptionPerYear({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_daily'
  const field = !isReactive ? 'consumption' : 'reactiveConsumption'
  const query =
    `SELECT 
    year,
    '${field}' AS metric,
    SUM(${field}) AS total,
  '${accumulatedName}' AS location,
  loadStoringTime,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllYears({ date })})
  GROUP BY
    year,
    loadStoringTime
  ORDER BY
    year ASC`
  return query
}

module.exports.findConsumptionComparePerYear = function findConsumptionComparePerYear({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_daily'
  const query = `SELECT
    elementId,
    identificationCode,
    consumption,
    year,
    'consumption' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((${getAllYears({ date: rangeOne })})
  OR (${getAllYears({ date: rangeTwo })}))
  ORDER BY
    year ASC`
  return runQuery(query)
}

function buildQueryAccumulatedCompareConsumptionPerYear({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_daily'
  const query =
    `SELECT
    year,
    loadStoringTime,
    'consumption' AS metric,
    SUM(consumption) AS total,
  '${accumulatedName}' AS location,
  SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ((${getAllYears({ date: rangeOne })})
  OR (${getAllYears({ date: rangeTwo })}))
  GROUP BY
    loadStoringTime,
    year
  ORDER BY
    year ASC`
  return query
}

function buildQueryAccumulatedLoadProfilePerYear({ dataSetName, elementsId, accumulatedName, date, isReactive }) {
  const tableName = 'consumption_profile_annual'
  const field = !isReactive ? 'consumptionProfile' : 'reactiveConsumptionProfile'
  const query =
    `SELECT
    year,
    '${field}' AS metric,
    SUM(${field}) AS total,
    '${accumulatedName}' AS location,
    SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ${field} IS NOT NULL
  AND (${getAllYears({ date, withOutDay: true })})
  GROUP BY
    year
  ORDER BY
    year ASC`
  return query
}

module.exports.findLoadProfileComparePerYear = function findLoadProfileComparePerYear({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'consumption_profile_annual'
  const query = `SELECT *, 'consumptionProfile' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((${getAllYears({ date: rangeOne, withOutDay: true })})
  OR (${getAllYears({ date: rangeTwo, withOutDay: true })}))
  ORDER BY
   year ASC`

  return runQuery(query)
}

function buildQueryAccumulatedCompareLoadProfilePerYear({ dataSetName, elementsId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'consumption_profile_annual'
  const query =
    `SELECT
    year,
    'consumptionProfile' AS metric,
    SUM(consumptionProfile) AS total,
    '${accumulatedName}' AS location,
    SPLIT(STRING_AGG(elementId, ','), ',') as elementsId
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND ((${getAllYears({ date: rangeOne, withOutDay: true })})
  OR (${getAllYears({ date: rangeTwo, withOutDay: true })}))
  GROUP BY
    year
  ORDER BY
    year ASC`
  return query
}

// VIBRATION HOUR

function getWhereForStartAndEndTime(start, end) {
  if (!start || !end) {
    return ''
  }
  const startSplit = start.split(':')
  const endSplit = end.split(':')
  return `
    AND (TIME(loadStoringTime) BETWEEN TIME(${startSplit[0]}, ${startSplit[1]}, 00)
      AND TIME(${endSplit[0]}, ${endSplit[1]}, 60))
  `
}

module.exports.findVibrationPerHourLimited = function findVibrationPerHourLimited({ dataSetName, elementsId, startDate, endDate, startTime, endTime, fields, limit }) {
  const tableName = 'warehouse'
  const query = `SELECT
    *
  FROM (
    SELECT
      elementId,
      timestamp AS loadStoringTime,
      ${fields},
      'vibration' AS metric,
      MOD(ROW_NUMBER() OVER (), 1024) AS valid
    FROM
    \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE
      elementId IN (${getElementsIdForQuery(elementsId)})
      AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
      AND identificationCode = 'vibration'
      ${getWhereForStartAndEndTime(startTime, endTime)}
    ORDER BY
      timestamp)
  WHERE
    valid = 0
  ${limit ? `LIMIT ${limit}` : ''}`
  return runQuery(query)
}

module.exports.findVibrationPerHour = function findVibrationPerHour({ dataSetName, elementsId, startDate, endDate, startTime, endTime, fields, limit }) {
  const tableName = 'warehouse'
  const query = `SELECT elementId, timestamp AS loadStoringTime, ${fields}, 'vibration' as metric
  FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE elementId IN (${getElementsIdForQuery(elementsId)})
  AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  AND identificationCode = 'vibration'
  ${getWhereForStartAndEndTime(startTime, endTime)}
   ORDER BY
   timestamp
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
}

module.exports.compareYesterdayLastConsumption = function compareYesterdayLastConsumption({ dataSetName, elementsId, yesterdayDate, dates }) {
  const formatDate = 'YYYY-MM-DD'
  const tableName = 'consumption_profile_daily'
  const query = `SELECT
   ((yesterday.consumptionProfile / avg.avg)-1)*100 AS porcentaje,
   yesterday.elementId
  FROM (
    SELECT *
    FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE elementId IN (${getElementsIdForQuery(elementsId)})
      AND loadStoringTime = '${yesterdayDate.format(formatDate)}') AS yesterday,
    (
      SELECT elementId, AVG(consumptionProfile) AS avg
      FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
      WHERE elementId IN (${getElementsIdForQuery(elementsId)})
        AND loadStoringTime IN (${getDateArrayInSqlFormat({ dates })})
      GROUP BY
        elementId
    ) AS avg
  WHERE yesterday.elementId = avg.elementId`
  return runQuery(query)
}

module.exports.findDayConsumptionProfileOnSpecificDates = function findDayConsumptionProfileOnSpecificDates({ dataSetName, elementsId, dates }) {
  const tableName = 'consumption_profile_daily'
  const query = `SELECT
    elementId,
    consumptionProfile,
    loadStoringTime
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    elementId IN (${getElementsIdForQuery(elementsId)})
    AND loadStoringTime IN (${getDateArrayInSqlFormat({ dates })})`
  return runQuery(query)
}

module.exports.findValueEnergyLastFiveDays = function findValueEnergyLastFiveDays({ dataSetName, elementsId, startDate, endDate }) {
  const tableName = 'consumption_profile_daily'
  const query = `SELECT
    elementId,
    consumptionProfile,
    loadStoringTime
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    elementId IN (${getElementsIdForQuery(elementsId)})
    AND loadStoringTime (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  ORDER BY loadStoringTime DESC
  `
  return runQuery(query)
}

module.exports.findByElementsIdAndLoadStoringTime = function findByElementsIdAndLoadStoringTime({
  dataSetName,
  elementsId,
  loadStoringTime,
  tableName = 'warehouse',
  typeLoadStoringTime = 'DATETIME',
  formatLoadStoringTime = 'YYYY, M, D, H, m, s',
  fields = 'elementId, consumption, loadStoringTime'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    elementId IN (${getElementsIdForQuery(elementsId)})
    AND loadStoringTime = ${typeLoadStoringTime}(${loadStoringTime.format(formatLoadStoringTime)})
  `
  return runQuery(query)
}

module.exports.findByElementsIdAndWeekAndYear = function findByElementsIdAndWeekAndYear({
  dataSetName,
  elementsId,
  tableName = 'warehouse',
  week,
  year,
  fields = 'elementId, consumptionProfile, week, year'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    elementId IN (${getElementsIdForQuery(elementsId)})
    AND year=${year} AND week=${week}
  `
  return runQuery(query)
}

module.exports.findByElementsIdAndMonthAndYear = function findByElementsIdAndMonthAndYear({
  dataSetName,
  elementsId,
  tableName = 'warehouse',
  month,
  year,
  fields = 'elementId, consumptionProfile, month, year'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    elementId IN (${getElementsIdForQuery(elementsId)})
    AND year=${year} AND month=${month}
  `
  return runQuery(query)
}

module.exports.findByElementsIdYear = function findByElementsIdYear({
  dataSetName,
  elementsId,
  tableName = 'warehouse',
  year,
  fields = 'elementId, consumptionProfile, year'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    elementId IN (${getElementsIdForQuery(elementsId)})
    AND year=${year}
  `
  return runQuery(query)
}


module.exports.findProjectionLinearRegByElementId = function findProjectionLinearRegByElementId({
  dataSetName,
  tableName = 'consumption_profile_monthly',
  elementsId,
  dates
}) {
  const query = `
          WITH
          equation AS (
          WITH
            result AS (
            SELECT
              ROW_NUMBER() OVER() x,
              y
            FROM (
              SELECT
                consumptionProfile AS y,
                *
              FROM (
                SELECT
                  SUM(consumptionProfile) AS consumptionProfile,
                  year,
                  month
                FROM
                \`${getFrom({ projectId, dataSetName, tableName })}\`
                WHERE
                  elementId IN (${getElementsIdForQuery(elementsId)})
                  AND consumptionProfile > 0
                  AND (${getDateArrayInMonthAndYear({ dates })})
                GROUP BY
                  year,
                  month
                ORDER BY
                  year DESC,
                  month DESC
              )
              ORDER BY
                year ASC,
                month ASC ) )
          SELECT
            slope,
            y_bar_max - x_bar_max * slope AS intercept
          FROM (
            SELECT
              SUM((x - x_bar) * (y - y_bar)) / SUM((x - x_bar) * (x - x_bar)) AS slope,
              MAX(x_bar) AS x_bar_max,
              MAX(y_bar) AS y_bar_max
            FROM (
              SELECT
                x, AVG(x) OVER () AS x_bar,
                y, AVG(y) OVER () AS y_bar
              FROM
                result) s ) )
        SELECT slope,  intercept FROM equation
          `
  return runQuery(query)
    .catch((e) => {
      console.log('error: ', e)
      return [];
    })
}

module.exports.findDaysWithEvents = function findDaysWithEvents({ dataSetName, locationName, elementsId, startDate, endDate }) {
  const tableName = 'consumption_profile_hour'
  const percentageEvent = 0.5
  const query = `
    SELECT
    '${locationName}' AS location,
       loadStoringTime,
       SPLIT(STRING_AGG(elementId, ','), ',') AS elementId
    FROM
    \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE
      elementId IN (${getElementsIdForQuery(elementsId)})
      AND ROUND(reactiveConsumptionProfile,1) > (ROUND(consumptionProfile,1) * ${percentageEvent})
      AND DATE(loadStoringTime) BETWEEN DATE('${moment(startDate).format('YYYY-MM-DD')}') AND DATE('${moment(endDate).format('YYYY-MM-DD')}')
    GROUP BY
      loadStoringTime
    ORDER BY
      loadStoringTime 
  `
  return runQuery(query)
}

module.exports.findMonthFactor = function findMonthFactor({ dataSetName, locationName, elementsId, dates }) {
  const tableName = 'multiplier_factor'
  const query = `
  SELECT
  '${locationName}' AS location,
  MAX(factor) AS factor,
  month,
  year,
  SPLIT(STRING_AGG(elementId, ','), ',') AS elementsId
FROM
\`${getFrom({ projectId, dataSetName, tableName })}\`
WHERE
  elementId IN (${getElementsIdForQuery(elementsId)})
  AND (${getDateArrayInMonthAndYear({ dates })})
    GROUP BY
      month,
      year
    ORDER BY
      year,
      month
  `
  return runQuery(query)
}

module.exports.findValuesIndicatorByMonthAndYear = function findValuesIndicatorByMonthAndYear({
  dataSetName,
  locationId,
  indicatorId,
  tableName = 'indicator_monthly',
  year,
  fields = 'locationId, elementId, indicatorId, value, month, year'
}) {
  const query = `SELECT
  ${fields}
  FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  WHERE
    locationId IN (${getElementsIdForQuery(locationId)})
    AND indicatorId = '${indicatorId}'
    AND year=${year}
    ORDER BY month
  `
  return runQuery(query)
}

module.exports.findTrackingUser = function findTrackingUser({
  dataSetName,
  tableName,
}) {
  const query = `SELECT
  * FROM
  \`${getFrom({ projectId, dataSetName, tableName })}\`
  `
  return runQuery(query)
}

module.exports.findIndicatorsPerMonth = function findIndicatorsPerMonth({ dataSetName, elementsId, date, indicatorId, locationsId,limit }) {
  const tableName = 'indicator_monthly'
  let query = null

  if (elementsId && elementsId.length) {
    query = `
    SELECT * FROM 
          \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE
        indicatorId IN  (${getElementsIdForQuery(indicatorId)})
        AND 
        elementId IN (${getElementsIdForQuery(elementsId)}) 
        AND (${getDateArrayInMonthAndYear({ dates: date })})`
  } else if (locationsId && locationsId.length) {
    query = `
    SELECT DISTINCT locationId, indicatorId, value, month, year  FROM
          \`${getFrom({ projectId, dataSetName, tableName })}\`
    WHERE
        indicatorId = '${indicatorId}'
        AND 
        locationId IN (${getElementsIdForQuery(locationsId)}) 
        AND (${getDateArrayInMonthAndYear({ dates: date })})
        `
  }
  if (limit > 0) {
    query = `${query} LIMIT ${limit}`
  }
  return runQuery(query)
}
module.exports.findTrackingByEnvironment = function findTrackingByEnvironment({
  dataSetName,
  tableName,
  environment,
  module,
  date = null
}) {
  const query = `SELECT 
    * FROM 
    \`${getFrom({ projectId, dataSetName, tableName })}\`
    where module in (${joinItemsWithCommaForQuery(module)}) 
    ${date ? `AND DATE(createdAt) = DATE('${date}')` : ''}
    and environment =  '${environment}'
    ORDER BY createdAt ASC
    `
  return runQuery(query)
}

function getProjectId() {
  return projectId
}
function buildQueryAccumulatedCompareIndicatorPerMonth({
  dataSetName, locationId, indicatorId, rangeOne, rangeTwo, accumulatedName }) {
  const tableName = 'indicator_monthly'
  const query =
    `SELECT 
     locationId,
     month,
     year,
     'indicator' AS metric,
     value,
    '${accumulatedName}' AS location,
FROM \`${getFrom({ projectId, dataSetName, tableName })}\`
WHERE locationId IN (${getElementsIdForQuery(locationId)})
AND indicatorId IN (${joinItemsWithCommaForQuery(indicatorId)})
AND (${getDateArrayInMonthAndYear({ dates: rangeOne })}
     OR ${getDateArrayInMonthAndYear({ dates: rangeTwo })})
`
  return query
}
module.exports.getProjectId = getProjectId