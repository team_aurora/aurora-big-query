// Helpers
const { getFormatDateSqlByGranularity } = require('aurora-bigquery/lib/helpers/granularityHelpers')
const { joinItemsWithCommaForQuery } =  require('aurora-bigquery/lib/helpers/dataHelpers')

// bigQuery
const { runQuery, getFrom, getProjectId } = require('aurora-bigquery/lib/bigQuery')

// This query gets tempeture per hour between a date range 
module.exports.findTemperaturePerHour = function findTemperaturePerHour({ dataSetName, elementsId, startDate, endDate, limit }) {
  const tableName = 'temperature'
  const query = `
  SELECT DISTINCT 
    elementId, 
    DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime, 
    temperature, 
    'temperature' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  ORDER BY
    loadStoringTime 
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
}

// This query gets tempeture per Instantanios between a date range 
module.exports.findTemperaturePerInstantaneous = function findTemperaturePerInstantaneous({ dataSetName, elementsId, startDate, endDate, limit }) {
  const tableName = 'temperature'
  const query = `
  SELECT DISTINCT 
    elementId, 
    DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime, 
    temperature, 
    'temperature' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  ORDER BY
    loadStoringTime 
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
}

// This query gets temperature per hour between two date ranges
module.exports.findTemperatureComparePerHour = function findTemperatureComparePerHour({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'temperature'
  const query = `SELECT DISTINCT elementId, loadStoringTime, temperature, 'temperature' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
   ORDER BY
   loadStoringTime`

  return runQuery(query)
}

// This query gets tempeture per day, week monthly and between a date range 
module.exports.findTemperaturePerDayAndWeekAndMonthAndYear = function findTemperaturePerDayAndWeekAndMonthAndYear({ dataSetName, elementsId, startDate, endDate, granularity, limit }) {
  const tableName = 'temperature'
  const formatDate = getFormatDateSqlByGranularity(granularity)
  const query = `
  SELECT elementId,
      AVG(temperature) AS temperature,
      MAX(temperature ) AS max,
      MIN(temperature ) AS min,
      FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
      'temperature' AS metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  GROUP BY
    elementId,
    loadStoringTime
   ORDER BY
    loadStoringTime DESC 
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
}

// This query gets tempeture last minutes
module.exports.findTemperatureInLastMinutes = function findTemperatureInLastMinutes({ dataSetName, elementsId, numberMinutes }) {
  const tableName = 'temperature'
  const timeZone = '-05:00'

  const query = `SELECT DISTINCT 
    elementId,
    loadStoringTime,
    temperature
  FROM
  \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE
    elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    AND temperature IS NOT NULL
    AND loadStoringTime BETWEEN DATETIME_SUB(CURRENT_DATETIME('${timeZone}'),
      INTERVAL ${numberMinutes} MINUTE)
    AND CURRENT_DATETIME('${timeZone}')`
  return runQuery(query)
}

// This query gets the last temperature
module.exports.getLastTemperatureValue = function getLastTemperatureValue({ dataSetName, elementsId }) {
  const tableName = 'temperature'
  const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
  const query = `
  SELECT DISTINCT t.elementId, t.temperature, t.loadStoringTime
  FROM 
    \`${from}\` as t,
    (
      SELECT
      elementId, MAX(loadStoringTime) AS loadStoringTime
      FROM
      \`${from}\`
      WHERE
        elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      GROUP BY 
      elementId
    ) as lastValues
  WHERE
    t.elementId = lastValues.elementId AND t.loadStoringTime = lastValues.loadStoringTime
  `
  return runQuery(query)
}

// This query gets the temperature 
module.exports.getTemperature = function getTemperature({ dataSetName, elementsId, startDate, endDate, format, stringFormat }) {
  const tableName = 'temperature'
  const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
  const query = `
  SELECT 
  elementId,
  temperature,
  FORMAT_DATE("%Y-%m-%d %H:%M:%S", loadStoringTime) loadStoringTime,
  EXTRACT(DAY FROM loadStoringTime) monthDay
  FROM \`${from}\` -- Tener en cuenta el nombre de la compañía
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)}) 
   AND loadStoringTime 
    BETWEEN '${format(startDate, stringFormat)}'
    AND '${format(endDate, stringFormat)}'
  ORDER BY loadStoringTime ASC `
  return runQuery(query)
}

module.exports.findRegisterDaily = function findRegisterDaily({ dataForQuery, format, stringFormat, dataSetName }) {
  const tableName = 'temperature'
  const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
  const query = `
      SELECT 
      elementId, 
      '[TIME]' time,
      '[HOUR]' hour,
      [MONTH_DAY] monthDay,
      '[PERIOD]' period, 
      max(temperature) temperatureMaximum, 
      min(temperature) temperatureMinimum,
      FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S", max(actual.loadStoringTimeActual))  loadStoringTimeMoment,
      max(actual.temperatureActual) temperatureMoment,
      FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S", max(enviromentMoment.loadStoringTimeActual)) enviromentloadStoringTimeMoment,
      max(enviromentMoment.temperatureActual) enviromentTemperaturaMoment
    FROM \`${from}\`,
    (             SELECT DISTINCT temperature as temperatureActual, loadStoringTime as loadStoringTimeActual
            FROM \`${from}\` 
            WHERE elementId = '[ELEMENT_ID]'
            AND loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
            ORDER BY loadStoringTime DESC
            LIMIT 1
      ) actual,
      (
        select * from (select '[ENVRINMENT_ELEMENT_ID]')
        full outer join 
        (  SELECT DISTINCT temperature as temperatureActual, loadStoringTime as loadStoringTimeActual
            FROM \`${from}\` 
            WHERE elementId = '[ENVRINMENT_ELEMENT_ID]'
            AND loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
            ORDER BY loadStoringTime DESC
            LIMIT 1) on 1 = 1
      ) enviromentMoment
    WHERE elementId = '[ELEMENT_ID]'
    and loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
    group by 1
  `

  const queryWithData = dataForQuery.map( (dayItem) => {
    return `
       ${query
        .replace('[TIME]', format(dayItem.morning.time, stringFormat))
        .replace('[MONTH_DAY]', dayItem.monthDay)
        .replace('[HOUR]', dayItem.morning.hour)
        .replace('[PERIOD]', 'M')
        .split('[ELEMENT_ID]').join(dayItem.elementId)
        .split('[ENVRINMENT_ELEMENT_ID]').join(dayItem.environmentElementId) 
        .split('[START_TIME]').join(format(dayItem.morning.startTime, stringFormat)) 
        .split('[END_TIME]').join(format(dayItem.morning.endTime, stringFormat)) 
      }
       UNION ALL
       ${query
        .replace('[TIME]', format(dayItem.afternoon.time, stringFormat))
        .replace('[MONTH_DAY]', dayItem.monthDay)
        .replace('[HOUR]', dayItem.afternoon.hour)
        .replace('[PERIOD]', 'T')
        .split('[ELEMENT_ID]').join(dayItem.elementId)
        .split('[ENVRINMENT_ELEMENT_ID]').join(dayItem.environmentElementId) 
        .split('[START_TIME]').join(format(dayItem.afternoon.startTime, stringFormat)) 
        .split('[END_TIME]').join(format(dayItem.afternoon.endTime, stringFormat))}
    `
  }).join('UNION ALL \n')
  const orderBy = ' ORDER BY monthDay ASC, period ASC '
  const queryComplete = queryWithData.concat(orderBy)

  return runQuery(queryComplete)  
}

// This query gets last temperature by elementIds
module.exports.lastTemperatureByElementIds = function lastTemperatureByElementIds({ dataSetName, elementsId }) {
  const tableName = 'temperature'
  const query = `
  SELECT elementId, 
  ARRAY_AGG(STRUCT(loadStoringTime, temperature) ORDER BY loadStoringTime DESC LIMIT 1)[offset(0)] value
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)}) 
  AND temperature IS NOT NULL
  GROUP BY elementId
  `
  return runQuery(query)
}

// This query gets data for device temperature control periods report
module.exports.findDeviceTemperatureControl = function findDeviceTemperatureControl({ dataForQuery, format, stringFormat, dataSetName }) {
  const tableName = 'temperature'
  const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
  const query = `
    SELECT *
    FROM (
       SELECT DISTINCT
          '[ELEMENT_ID]' elementId, 
          '[TIME]' time,
          '[HOUR]' hour,
          [MONTH_DAY] monthDay,
          '[PERIOD]' period,
          temperature, 
          loadStoringTime
    FROM \`${from}\`
     WHERE elementId = '[ELEMENT_ID]'
     and loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
     ORDER BY loadStoringTime ASC
     LIMIT 1
    )
  `

  const queryWithData = dataForQuery.map( (dayItem) => {
    return `
       ${query
        .replace('[TIME]', format(dayItem.morning.time, stringFormat))
        .replace('[MONTH_DAY]', dayItem.monthDay)
        .replace('[HOUR]', dayItem.morning.hour)
        .replace('[PERIOD]', 'MORNING')
        .split('[ELEMENT_ID]').join(dayItem.temperatureElementId)
        .split('[START_TIME]').join(format(dayItem.morning.startDate, stringFormat)) 
        .split('[END_TIME]').join(format(dayItem.morning.endDate, stringFormat)) 
      }
       UNION ALL
       ${query
        .replace('[TIME]', format(dayItem.noon.time, stringFormat))
        .replace('[MONTH_DAY]', dayItem.monthDay)
        .replace('[HOUR]', dayItem.noon.hour)
        .replace('[PERIOD]', 'NOON')
        .split('[ELEMENT_ID]').join(dayItem.temperatureElementId)
        .split('[START_TIME]').join(format(dayItem.noon.startDate, stringFormat)) 
        .split('[END_TIME]').join(format(dayItem.noon.endDate, stringFormat))
      }
      UNION ALL
      ${query
       .replace('[TIME]', format(dayItem.afternoon.time, stringFormat))
       .replace('[MONTH_DAY]', dayItem.monthDay)
       .replace('[HOUR]', dayItem.afternoon.hour)
       .replace('[PERIOD]', 'AFTERNOON')
       .split('[ELEMENT_ID]').join(dayItem.temperatureElementId)
       .split('[START_TIME]').join(format(dayItem.afternoon.startDate, stringFormat)) 
       .split('[END_TIME]').join(format(dayItem.afternoon.endDate, stringFormat))
     }
    `
  }).join('UNION ALL \n')
  const orderBy = ' ORDER BY monthDay ASC, hour ASC '
  const queryComplete = queryWithData.concat(orderBy)

  return runQuery(queryComplete)  
}

// This query gets data for device temperature control avg by day report
module.exports.findDeviceTemperatureControlAvg = function findDeviceTemperatureControlAvg({ 
  startMonth,
  endMonth,
  elementId,
  format,
  stringFormat,
  dataSetName
}) {
  const tableName = 'temperature'
  const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
  const query = `
    SELECT DISTINCT
          elementId, 
          EXTRACT(DAY FROM loadStoringTime) monthDay,
          AVG(temperature) temperatureAvg,
          MAX(temperature) temperatureMax,
          MIN(temperature) temperatureMin
    FROM \`${from}\`
    WHERE elementId = '${elementId}'
    AND loadStoringTime BETWEEN
       '${format(startMonth, stringFormat)}' 
       AND '${format(endMonth, stringFormat)}' 
    GROUP BY elementId, monthDay
    ORDER BY monthDay ASC
  `
  return runQuery(query)  
}

// This query gets temperature per instantaneous between two date ranges
module.exports.findTemperatureComparePerInstantaneous = function findTemperatureComparePerInstantaneous({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'temperature'
  const query = `SELECT DISTINCT elementId, loadStoringTime, temperature, 'temperature' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
   ORDER BY
   loadStoringTime`

  return runQuery(query)
}

/**
 * This query gets temperature per granularities between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   temperature,
 *   max,
 *   min,
 * },
 * .....
 * ]
 */
 module.exports.findTemperatureComparePerHourAndDayAndWeekAndMonthAndYear = 
 function findTemperatureComparePerHourAndDayAndWeekAndMonthAndYear({ 
  dataSetName, elementId, granularity, limit, metric = 'temperature', rangeOne, rangeTwo }) {
    const tableName = 'temperature'
    const formatDate = getFormatDateSqlByGranularity(granularity)
    const query = `
    SELECT elementId,
        AVG(temperature) AS temperature,
        MAX(temperature) AS max,
        MIN(temperature) AS min,
        FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
        '${metric}' AS metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementId}' 
      AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
      OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
    GROUP BY
      elementId,
      loadStoringTime
     ORDER BY
      loadStoringTime DESC 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

 /**
  * Gets the last value and connection
  * 
  * @param {string} dataSetName: Name of the company
  * @param {array} elementIds: Array of elements id
  *
  * @returns {array} Last rows
  */
  module.exports.findLastConnectionValueTemperature = function findLastConnectionValueTemperature({ dataSetName, elementsId }) {
    const tableName = 'temperature'
    const query = `
    SELECT
     elementId,
     lastRow.temperature lastValue,
     lastRow.loadStoringTime lastConnection
    FROM (
      SELECT
        elementId,
        ARRAY_AGG(STRUCT( temperature, loadStoringTime)
        ORDER BY
        loadStoringTime DESC
        LIMIT 1)[OFFSET(0)] lastRow
      FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE
     elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    GROUP BY 1 )
    `
    return runQuery(query)
  }