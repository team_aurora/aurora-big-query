// Helpers
const { getFormatDateSqlByGranularity } = require('aurora-bigquery/lib/helpers/granularityHelpers')
const { joinItemsWithCommaForQuery } =  require('aurora-bigquery/lib/helpers/dataHelpers')

// bigQuery
const { runQuery, getFrom, getProjectId } = require('aurora-bigquery/lib/bigQuery')

// Query gets the humidity per hour between two date ranges
module.exports.findHumidityComparePerHour = function findHumidityComparePerHour({ dataSetName, elementId, rangeOne, rangeTwo }) {
    const tableName = 'humidity'
    const query = `
    SELECT DISTINCT 
        elementId, 
        DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime,
        humidity, 
        'humidity' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementId}'
    AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
    OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
     ORDER BY
     loadStoringTime DESC`
  
    return runQuery(query)
  }

// This query gets humidity per day, week monthly and between a date range 
  module.exports.findHumidityPerDayAndWeekAndMonthAndYear = function findHumidityPerDayAndWeekAndMonthAndYear({ dataSetName, elementsId, startDate, endDate, granularity, limit }) {
    const tableName = 'humidity'
    const formatDate = getFormatDateSqlByGranularity(granularity)
    const query = `
    SELECT 
        elementId,
        AVG(humidity) AS humidity,
        MAX(humidity ) AS max,
        MIN(humidity ) AS min,
        FORMAT_DATETIME('${formatDate}',
          loadStoringTime) AS loadStoringTime,
        'humidity' AS metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    GROUP BY
      elementId,
      loadStoringTime
     ORDER BY
      loadStoringTime
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

  // This query gets the last humidity
  module.exports.findHumidityInLastMinutes = function findHumidityInLastMinutes({ dataSetName, elementsId, numberMinutes }) {
    const tableName = 'humidity'
    const timeZone = '-05:00'
  
    const query = `
    SELECT DISTINCT 
        elementId,
        loadStoringTime,
        humidity
    FROM
    \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE
      elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      AND humidity IS NOT NULL
      AND loadStoringTime BETWEEN DATETIME_SUB(CURRENT_DATETIME('${timeZone}'),
        INTERVAL ${numberMinutes} MINUTE)
      AND CURRENT_DATETIME('${timeZone}')`
    return runQuery(query)
  }

  // This query gets the last temperature
  module.exports.getLastHumidityeValue = function getLastHumidityeValue({ dataSetName, elementsId }) {
    const tableName = 'humidity'
    const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
    const query = `
    SELECT  DISTINCT t.elementId, t.humidity, t.loadStoringTime
    FROM 
      \`${from}\` as t,
      (
        SELECT
        elementId, MAX(loadStoringTime) AS loadStoringTime
        FROM
        \`${from}\`
        WHERE
          elementId IN (${joinItemsWithCommaForQuery(elementsId)})
        GROUP BY 
        elementId
      ) as lastValues
    WHERE
      t.elementId = lastValues.elementId AND t.loadStoringTime = lastValues.loadStoringTime
    `
    return runQuery(query)
  }
// This query gets humidity per hour between a date range 
module.exports.findHumidityPerHour = function findHumidityPerHour({ dataSetName, elementsId, startDate, endDate, limit }) {
    const tableName = 'humidity'
    const query =  `
    SELECT DISTINCT 
        elementId, 
        DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
        EXTRACT(MONTH FROM loadStoringTime), 
        EXTRACT(DAY FROM loadStoringTime), 
        EXTRACT(HOUR FROM loadStoringTime), 
        EXTRACT(MINUTE FROM loadStoringTime), 
        0
        ) loadStoringTime,
        humidity, 
        'humidity' as metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
    ORDER BY
      loadStoringTime
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

// This query gets humidity per instantanious between a date range 
module.exports.findHumidityPerInstantaneous = function findHumidityPerInstantaneous({ dataSetName, elementsId, startDate, endDate, limit }) {
  const tableName = 'humidity'
  const query =  `
  SELECT DISTINCT 
      elementId, 
      DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
      EXTRACT(MONTH FROM loadStoringTime), 
      EXTRACT(DAY FROM loadStoringTime), 
      EXTRACT(HOUR FROM loadStoringTime), 
      EXTRACT(MINUTE FROM loadStoringTime), 
      0
      ) loadStoringTime,
      humidity, 
      'humidity' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
    AND (DATE(loadStoringTime) BETWEEN DATE('${startDate}') AND DATE('${endDate}'))
  ORDER BY
    loadStoringTime
  ${limit ? `LIMIT ${limit}` : ''}`

  return runQuery(query)
}



// This query gets the temperature and humidity for the daily recording report
module.exports.findHumidityAndTemperatureDailyRecording = function findHumidityAndTemperatureDailyRecording({ dataForQuery, format, stringFormat, dataSetName }) {
  const tableName = 'temperature'
  const from = getFrom({ projectId: getProjectId(), dataSetName, tableName })
  const fromHumidity = getFrom({ projectId: getProjectId(), dataSetName, tableName: 'humidity' })
  const query = `
      SELECT 
      '[HUMIDITY_ELEMENT_ID]' elementId, 
      '[TIME]' time,
      '[HOUR]' hour,
      [MONTH_DAY] monthDay,
      '[PERIOD]' period, 
      max(temperature) temperatureMaximum, 
      min(temperature) temperatureMinimum,
      FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S", max(actual.loadStoringTimeActual))  loadStoringTimeMoment,
      max(actual.temperatureActual) temperatureMoment,
      FORMAT_DATETIME("%Y-%m-%dT%H:%M:%S", max(humidityMoment.loadStoringTimeActual)) humidityloadStoringTimeMoment,
      max(humidityMoment.humidiyActual) humidityMoment
    FROM \`${from}\`,
    (             SELECT DISTINCT temperature as temperatureActual, loadStoringTime as loadStoringTimeActual
            FROM \`${from}\` 
            WHERE elementId = '[ELEMENT_ID]'
            AND loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
            ORDER BY loadStoringTime DESC
            LIMIT 1
      ) actual,
      (
        select * from (select '[HUMIDITY_ELEMENT_ID]')
        full outer join 
        (  SELECT DISTINCT humidity as humidiyActual, loadStoringTime as loadStoringTimeActual
            FROM \`${fromHumidity}\` 
            WHERE elementId = '[HUMIDITY_ELEMENT_ID]'
            AND loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
            ORDER BY loadStoringTime DESC
            LIMIT 1) on 1 = 1
      ) humidityMoment
    WHERE elementId = '[ELEMENT_ID]'
    and loadStoringTime BETWEEN  DATETIME '[START_TIME]' AND DATETIME '[END_TIME]'
    group by 1
  `

  const queryWithData = dataForQuery.map( (dayItem) => {
    return `
       ${query
        .replace('[TIME]', format(dayItem.morning.time, stringFormat))
        .replace('[MONTH_DAY]', dayItem.monthDay)
        .replace('[HOUR]', dayItem.morning.hour)
        .replace('[PERIOD]', 'M')
        .split('[ELEMENT_ID]').join(dayItem.elementId)
        .split('[HUMIDITY_ELEMENT_ID]').join(dayItem.humiditytElementId) 
        .split('[START_TIME]').join(format(dayItem.morning.startTime, stringFormat)) 
        .split('[END_TIME]').join(format(dayItem.morning.endTime, stringFormat)) 
      }
       UNION ALL
       ${query
        .replace('[TIME]', format(dayItem.afternoon.time, stringFormat))
        .replace('[MONTH_DAY]', dayItem.monthDay)
        .replace('[HOUR]', dayItem.afternoon.hour)
        .replace('[PERIOD]', 'T')
        .split('[ELEMENT_ID]').join(dayItem.elementId)
        .split('[HUMIDITY_ELEMENT_ID]').join(dayItem.humiditytElementId) 
        .split('[START_TIME]').join(format(dayItem.afternoon.startTime, stringFormat)) 
        .split('[END_TIME]').join(format(dayItem.afternoon.endTime, stringFormat))}
    `
  }).join('UNION ALL \n')
  const orderBy = ' ORDER BY monthDay ASC, period ASC '
  const queryComplete = queryWithData.concat(orderBy)

  return runQuery(queryComplete)  
}

// This query gets last humidity by elementIds
module.exports.lastHumidityByElementIds = function lastTemperatureByElementIds({ dataSetName, elementsId }) {
  const tableName = 'humidity'
  const query = `
  SELECT elementId, 
  ARRAY_AGG(STRUCT(loadStoringTime, humidity) ORDER BY loadStoringTime DESC LIMIT 1)[offset(0)] value
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId IN (${joinItemsWithCommaForQuery(elementsId)})
  AND humidity IS NOT NULL
  GROUP BY elementId
  `
  return runQuery(query)
}


// Query gets the humidity per hour between two date ranges
module.exports.findHumidityComparePerInstantaneous = function findHumidityComparePerInstantaneous({ dataSetName, elementId, rangeOne, rangeTwo }) {
  const tableName = 'humidity'
  const query = `
  SELECT DISTINCT 
      elementId, 
      DATETIME(EXTRACT(YEAR FROM loadStoringTime), 
      EXTRACT(MONTH FROM loadStoringTime), 
      EXTRACT(DAY FROM loadStoringTime), 
      EXTRACT(HOUR FROM loadStoringTime), 
      EXTRACT(MINUTE FROM loadStoringTime), 
      0
      ) loadStoringTime,
      humidity, 
      'humidity' as metric
  FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
  WHERE elementId = '${elementId}'
  AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
  OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
   ORDER BY
   loadStoringTime DESC`

  return runQuery(query)
}


/**
 * This query gets humidity per granularities between two date range for compare
 * 
 * @param {string} dataSetName: Name of dataSet 
 * @param {string} elementsId: Id of element
 * @param {object} rangeOne: Object with startDate and enddate
 * @param {object} rangeTow: Object with startDate and enddate
 * @param {string} limit: Lmit of rows 
 * @param {string} metric: Name of metric 
 * @returns {Array} Rows with this structure: 
 * [
 * {
 *   elementId,
 *   loadStoringTime,
 *   humidity,
 *   max,
 *   min,
 * },
 * .....
 * ]
 */
 module.exports.findHumidityComparePerHourAndDayAndWeekAndMonthAndYear = 
 function findHumidityComparePerHourAndDayAndWeekAndMonthAndYear({ 
  dataSetName, elementId, granularity, limit, metric = 'humidity', rangeOne, rangeTwo }) {
    const tableName = 'humidity'
    const formatDate = getFormatDateSqlByGranularity(granularity)
    const query = `
    SELECT elementId,
        AVG(humidity) AS humidity,
        MAX(humidity) AS max,
        MIN(humidity) AS min,
        FORMAT_DATETIME('${formatDate}', loadStoringTime) AS loadStoringTime,
        '${metric}' AS metric
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE elementId = '${elementId}' 
      AND ((DATE(loadStoringTime) BETWEEN DATE('${rangeOne[0]}') AND DATE('${rangeOne[1]}')) 
      OR (DATE(loadStoringTime) BETWEEN DATE('${rangeTwo[0]}') AND DATE('${rangeTwo[1]}')))
    GROUP BY
      elementId,
      loadStoringTime
     ORDER BY
      loadStoringTime DESC 
    ${limit ? `LIMIT ${limit}` : ''}`
  
    return runQuery(query)
  }

   /**
  * Gets the last value and connection
  * 
  * @param {string} dataSetName: Name of the company
  * @param {array} elementIds: Array of elements id
  *
  * @returns {array} Last rows
  */
    module.exports.findLastConnectionValueHumidity = function findLastConnectionValueHumidity({ dataSetName, elementsId }) {
      const tableName = 'humidity'
      const query = `
      SELECT
       elementId,
       lastRow.humidity lastValue,
       lastRow.loadStoringTime lastConnection
      FROM (
        SELECT
          elementId,
          ARRAY_AGG(STRUCT( humidity, loadStoringTime)
          ORDER BY
          loadStoringTime DESC
          LIMIT 1)[OFFSET(0)] lastRow
        FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
      WHERE
       elementId IN (${joinItemsWithCommaForQuery(elementsId)})
      GROUP BY 1 )
      `
      return runQuery(query)
    }