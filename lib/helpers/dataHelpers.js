

function joinItemsWithCommaForQuery(items) {
  const total = items.length - 1;
  return items.reduce((result, elementId, index) => {
    result = result + `'${elementId}'`;
    if (index !== total) {
      result = result + ",";
    }
    return result;
  }, "")
}




module.exports = {
  joinItemsWithCommaForQuery
}