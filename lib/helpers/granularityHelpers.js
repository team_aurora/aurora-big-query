const moment = require('moment')

// TEMPERATURE HOUR, DAY, WEEK, MOMTH AND YEAR
function getFormatDateSqlByGranularity(granularity) {
    switch (granularity) {
      case 'hour':
        return '%Y-%m-%d %H'
      case 'day':
        return '%Y-%m-%d'
      case 'week':
        return '%G-%V'
      case 'month':
        return '%Y-%m'
      case 'year':
        return '%Y'
      default:
        return null
    }
  }

function buildQueryWeeks({ weekStart, weekEnd, year }) {
    return `(week BETWEEN ${weekStart}
    AND ${weekEnd}
    AND year = ${year})`
}

function getAllWeeks({ date }) {
    const weekByYear = {}
    let startDate = moment(date[0]).startOf('isoWeek')
    const endDate = moment(date[1]).endOf('isoWeek')
    let nextDate
    let nextYear
    while (startDate.isSameOrBefore(endDate)) {
      const year = startDate.isoWeekYear()
      const week = startDate.isoWeek()
      nextDate = startDate.clone().add(1, 'year').startOf('year')
      nextYear = nextDate.isoWeekYear()
      if (year === nextYear) {
        nextDate = nextDate.add(1, 'week').startOf('isoWeek')
        nextYear = nextDate.isoWeekYear()
      }
  
      if (!weekByYear[year]) {
        weekByYear[year] = [week]
      } else {
        weekByYear[year].push(week)
      }
      if (nextDate.isAfter(endDate)) {
        weekByYear[year].push(endDate.isoWeek())
      } else if (year !== nextYear) {
        weekByYear[year].push(startDate.clone().isoWeek(53).isoWeekYear() === year ? 53 : 52)
      }
      startDate = nextDate
    }
  
    const query = Object.keys(weekByYear)
      .reduce((results, year) => {
        const weekStart = weekByYear[year][0]
        const weekEnd = weekByYear[year].pop()
        if (results) {
          results += ' OR '
        }
        results += buildQueryWeeks({ weekStart, weekEnd, year })
        return results
      }, '')
  
    return query
  }

  function getAllYears({ date, withOutDay = false }) {
    let query = ''
    let diffYear = moment(date[1]).year() - moment(date[0]).year()
    let startDate = moment(date[0]).startOf('year')
    while (diffYear > -1) {
      const year = moment(startDate).year()
      query = query + buildQueryYears({ year, withOutDay })
      diffYear--
      query = diffYear > -1 ? `${query}  OR ` : query
      startDate = startDate.add(1, 'year')
    }
    return query
  }

  function getDateArrayInSqlFormat({ dates }) {
    const formatDate = 'YYYY-MM-DD'
    return dates.map((date) => {
      let format = `DATE('${date}')`
      if (typeof date === 'object') {
        format = `DATE('${date.format(formatDate)}')`
      }
      return format
    })
  }

  function buildQueryYears({ year, withOutDay }) {
    if (withOutDay) {
      return `(year = ${year})`
    }
    return `(year = ${year}
      AND day = 31 AND month = 12)`
  }

  function getDateArrayInMonthAndYear({ dates }) {
    const formatDate = 'YYYY-MM-DD'
    const startDate = moment(dates[0], formatDate).set("date",1).clone()
    const endDate = moment(dates[1], formatDate).set("date",1).clone()
    let results = ''
    while (startDate.isSameOrBefore(endDate)) {
      results += `(year = ${startDate.format('YYYY')} AND month = ${startDate.month() + 1})`
      startDate.add(1, 'month')
      if (startDate.isSameOrBefore(endDate)) {
        results += ' OR '
      }
    }
    return results
  }

  module.exports = {
    getFormatDateSqlByGranularity,
    getAllWeeks,
    getAllYears,
    getDateArrayInSqlFormat,
    getDateArrayInMonthAndYear
  }