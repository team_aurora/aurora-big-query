const { getFrom, getProjectId, runQuery } = require('./bigQuery')
const { joinItemsWithCommaForQuery } = require('./helpers/dataHelpers')

module.exports.findUserLastTracking = function findUserLastTracking({ dataSetName, emails }) {
  const tableName = 'user_tracking_complete'
  const query = `
  select email, lastTracking
  from (
    SELECT email, 
      ARRAY_AGG(STRUCT(createdAt, email) ORDER BY createdAt DESC LIMIT 1)[offset(0)].createdAt lastTracking
    FROM \`${getFrom({ projectId: getProjectId(), dataSetName, tableName })}\`
    WHERE email IN (${joinItemsWithCommaForQuery(emails)})
    GROUP BY 1 
    )
  `
  return runQuery(query)
}