const bigQuery = require('../lib/bigQuery');
const assert = require('chai').assert;


describe('BigQuery tests', () => {
  before(() => {
    const config = {
      gcloud: {
        projectId: 'projectIdForTest123',
        bigQueryCredentials: null
      }
    }
    bigQuery.init(config);
  });
  describe('#generateQuery()', () => {
    it('Check if the returned value has the correct format', async () => {
      const fields = ['id', 'name', 'password', 'email'];
      const dataSetName = 'dataset';
      const tableName = 'Person';
      const conditions = [
        {
            field: 'name',
            operator: '=',
            value: 'julian'
        },
        {
            field: 'id',
            operator: '=',
            value: '12345'
        }
      ];
      const limit = 200;
      const orderBy = ['name', 'email'];
      const precalculatedQuery = 'SELECT id,name,password,email FROM [projectIdForTest123:dataset.Person] WHERE (name = "julian") AND (id = "12345") ORDER BY name,email LIMIT 200';
      const query = await bigQuery.generateQuery(fields, dataSetName, tableName, conditions, limit, orderBy);
      assert.isString(query);
      assert.isNotEmpty(query);
      assert.isTrue(query.includes('LIMIT 200'));
      assert.equal(query, precalculatedQuery);
    });

    it('Check if the returned value has the correct format when has only one field', async () => {
      const fields = 'name';
      const dataSetName = 'dataset';
      const tableName = 'Person';
      const conditions = [
        {
            field: 'name',
            operator: '=',
            value: 'julian'
        },
        {
            field: 'id',
            operator: '=',
            value: '12345'
        }
      ];
      const limit = 200;
      const orderBy = ['name', 'email'];
      const precalculatedQuery = 'SELECT name FROM [projectIdForTest123:dataset.Person] WHERE (name = "julian") AND (id = "12345") ORDER BY name,email LIMIT 200';
      const query = await bigQuery.generateQuery(fields, dataSetName, tableName, conditions, limit, orderBy);
      assert.isString(query);
      assert.isNotEmpty(query);
      assert.isTrue(query.includes('LIMIT 200'));
      assert.equal(query, precalculatedQuery);
    });

    it('Check if the returned query has "undefined" whene the conditions are incorrect ', async () => {
      const fields = ['id', 'name', 'password', 'email'];
      const dataSetName = 'dataset';
      const tableName = 'Person';
      const conditions = [
        {
            field: 'name',
            operator: '=',
            value: 'julian'
        },
        {
            field: 'id',
            operator: '='
        }
      ];
      const limit = 200;
      const orderBy = ['name', 'email'];
      const query = await bigQuery.generateQuery(fields, dataSetName, tableName, conditions, limit, orderBy);
      assert.isString(query);
      assert.isNotEmpty(query);
      assert.isTrue(query.includes('undefined'));
    });

    it('Check if the default limit value is 100 ', async () => {
      const fields = ['id', 'name', 'password', 'email'];
      const dataSetName = 'dataset';
      const tableName = 'Person';
      const conditions = [
        {
            field: 'name',
            operator: '=',
            value: 'julian'
        },
        {
            field: 'id',
            operator: '=',
            value: '12345'
        }
      ];
      const orderBy = ['name', 'email'];
      const query = await bigQuery.generateQuery(fields, dataSetName, tableName, conditions, 0, orderBy);
      assert.isString(query);
      assert.isNotEmpty(query);
      assert.isTrue(query.includes('LIMIT 100'));
    });
  });
});