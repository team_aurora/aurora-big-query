# aurora-bigquery Package #


## Objective ##

**aurora-bigquery package** provides utilities for bigQuery

## Installation ##

### Usage ###

* Install the package
```
#!javascript
npm install https://bitbucket.org/team_aurora/aurora-big-query.git --save
```

* Require it in your code
```
#!javascript
const bigQuery = require('aurora-bigquery');
```